/********
 * career.js file (services/careers)
 ********/

const Career = require('../../models/career');
const fs = require("fs");

const addCareer = async (req, res, next) => {
    try {

        const {
            title,
            description
        } = req.body;

        if (title === undefined || title === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'title is required',
                'field': 'title'
            });
        }
        if (description === undefined || description === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'description is required',
                'field': 'description'
            });
        }

        if(req.files['image'] === undefined || req.files['image'] === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'image is required',
                'field': 'image'
            });
        }
        if(req.files['image1'] === undefined || req.files['image1'] === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'image1 is required',
                'field': 'image'
            });
        }

        let image = req.files['image'][0].path;
        let image1 = req.files['image1'][0].path;
        const temp = {
            title: title,
            description: description,
            file: image,
            file1: image1,
        }

        let client = await new Career(temp);

        client.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getCareer = async (req, res, next) => {
    try {

        let client = await Career.find({});

        if (client.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': client
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getCareerById = async (req, res, next) => {
    try {
        let client = await Career.findById(req.params.id);
        if (client) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': client
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateCareer = async (req, res, next) => {
    try {


        const clientId = req.params.id;

        const {
            title,
            description
        } = req.body;

        if (clientId === undefined || clientId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'clientId is required',
                'field': 'clientId'
            });
        }

        let update_client_value = {};

        if (title !== undefined && title !== ''){
            update_client_value.title = title;
        }
        if (description !== undefined && description !== ''){
            update_client_value.description = description;
        }
        if (req.files['image'] !== undefined){
            update_client_value.file = req.files['image'][0].path;
        }
        if (req.files['image1'] !== undefined){
            update_client_value.file1 = req.files['image1'][0].path;
        }

        let clientquery = { _id: clientId };
        // when true return the updated document
        let options = {new:true};

        let client_value = { $set: update_client_value };


        let isDataExists = await Career.findById(clientId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const clientPromise = Career.findOneAndUpdate(clientquery, client_value, options);

        Promise.all([clientPromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteCareer = async (req, res, next) => {
    try {
        let client = await Career.findByIdAndRemove(req.params.id);
        if (client) {
            fs.unlink(client.file, () =>{});

            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addCareer: addCareer,
    getCareer: getCareer,
    getCareerById: getCareerById,
    updateCareer: updateCareer,
    deleteCareer: deleteCareer
}
