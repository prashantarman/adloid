/********
 * slider.js file (services/sliders)
 ********/

const Slider = require('../../models/slider');
const fs = require("fs");

const addSlider = async (req, res, next) => {
    try {

        const {
            link
        } = req.body;

        if (link === undefined || link === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'link is required',
                'field': 'link'
            });
        }

        if(req.file === undefined || req.file === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'image is required',
                'field': 'image'
            });
        }
        let image = req.file.path;
        const temp = {
            link: link,
            file: image
        }

        let slider = await new Slider(temp);

        slider.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getSlider = async (req, res, next) => {
    try {

        let slider = await Slider.find({});

        if (slider.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': slider
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getSliderById = async (req, res, next) => {
    try {
        let slider = await Slider.findById(req.params.id);
        if (slider) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': slider
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateSlider = async (req, res, next) => {
    try {


        const SliderId = req.params.id;

        const {
            link
        } = req.body;

        if (SliderId === undefined || SliderId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'SliderId is required',
                'field': 'SliderId'
            });
        }

        let update_Slider_value = {};

        if (link !== undefined && link !== ''){
            update_Slider_value.link = link;
        }
        if (req.file !== undefined){
            update_Slider_value.file = req.file.path;
        }

        let Sliderquery = { _id: SliderId };
        // when true return the updated document
        let options = {new:true};

        let Slider_value = { $set: update_Slider_value };


        let isDataExists = await Slider.findById(SliderId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const SliderPromise = Slider.findOneAndUpdate(Sliderquery, Slider_value, options);

        Promise.all([SliderPromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteSlider = async (req, res, next) => {
    try {
        let slider = await Slider.findByIdAndRemove(req.params.id);
        if (slider) {
            fs.unlink(slider.file, () =>{});

            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addSlider: addSlider,
    getSlider: getSlider,
    getSliderById: getSliderById,
    updateSlider: updateSlider,
    deleteSlider: deleteSlider
}
