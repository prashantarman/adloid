/********
 * whyjoin.js file (services/whyjoin)
 ********/

const WhyJoin = require('../../models/whyjoin');

const addWhyJoin = async (req, res, next) => {
    try {

        const {
            question,
            answer
        } = req.body;

        if (question === undefined || question === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'question is required',
                'field': 'question'
            });
        }

        if (answer === undefined || answer === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'answer is required',
                'field': 'answer'
            });
        }

        const temp = {
            question: question,
            answer: answer
        }

        let caseStudy = await new WhyJoin(temp);

        caseStudy.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getWhyJoin = async (req, res, next) => {
    try {

        let casestudy = await WhyJoin.find({});

        if (casestudy.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': casestudy
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getWhyJoinById = async (req, res, next) => {
    try {
        let casestudy = await WhyJoin.findById(req.params.id);
        if (casestudy) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': casestudy
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateWhyJoin = async (req, res, next) => {
    try {
        const caseId = req.params.id;

        const {
            question,
            answer
        } = req.body;

        if (caseId === undefined || caseId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'caseId is required',
                'field': 'caseId'
            });
        }

        let update_casestudy_value = {};

        if (question !== undefined && question !== ''){
            update_casestudy_value.question = question;
        }
        if (answer !== undefined && answer !== ''){
            update_casestudy_value.answer = answer;
        }

        let casequery = { _id: caseId };
        // when true return the updated document
        let options = {new:true};

        let case_value = { $set: update_casestudy_value };


        let isDataExists = await WhyJoin.findById(caseId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const casePromise = WhyJoin.findOneAndUpdate(casequery, case_value, options);

        Promise.all([casePromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteWhyJoin = async (req, res, next) => {
    try {
        let casestudy = await WhyJoin.findByIdAndRemove(req.params.id);
        if (casestudy) {
            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `case with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addWhyJoin: addWhyJoin,
    getWhyJoin: getWhyJoin,
    getWhyJoinById: getWhyJoinById,
    updateWhyJoin: updateWhyJoin,
    deleteWhyJoin: deleteWhyJoin
}
