/********
 * currentOpening.js file (services/currentOpenings)
 ********/

const CurrentOpenings = require('../../models/currentopenings');

const addCurrentOpening = async (req, res, next) => {
    try {

        const {
            jobid,
            title,
            details
        } = req.body;

        if (jobid === undefined || jobid === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'jobid is required',
                'field': 'jobid'
            });
        }
        
        if (title === undefined || title === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'title is required',
                'field': 'title'
            });
        }

        if (details === undefined || details === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'details is required',
                'field': 'details'
            });
        }

        const temp = {
            jobid: jobid,
            title: title,
            details: details
        }

        let CurrentOpening = await new CurrentOpenings(temp);

        CurrentOpening.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getCurrentOpening = async (req, res, next) => {
    try {

        let CurrentOpening = await CurrentOpenings.find({});

        if (CurrentOpening.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': CurrentOpening
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getCurrentOpeningById = async (req, res, next) => {
    try {
        let CurrentOpening = await CurrentOpenings.findById(req.params.id);
        if (CurrentOpening) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': CurrentOpening
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateCurrentOpening = async (req, res, next) => {
    try {
        const caseId = req.params.id;

        const {
            title,
            jobid,
            details
        } = req.body;

        if (caseId === undefined || caseId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'caseId is required',
                'field': 'caseId'
            });
        }

        let update_CurrentOpening_value = {};

        if (jobid !== undefined && jobid !== ''){
            update_CurrentOpening_value.jobid = jobid;
        }
        if (title !== undefined && title !== ''){
            update_CurrentOpening_value.title = title;
        }
        if (details !== undefined && details !== ''){
            update_CurrentOpening_value.details = details;
        }

        let casequery = { _id: caseId };
        // when true return the updated document
        let options = {new:true};

        let case_value = { $set: update_CurrentOpening_value };


        let isDataExists = await CurrentOpenings.findById(caseId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const casePromise = CurrentOpenings.findOneAndUpdate(casequery, case_value, options);

        Promise.all([casePromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteCurrentOpening = async (req, res, next) => {
    try {
        let CurrentOpening = await CurrentOpenings.findByIdAndRemove(req.params.id);
        if (CurrentOpening) {
            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `case with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addCurrentOpening: addCurrentOpening,
    getCurrentOpening: getCurrentOpening,
    getCurrentOpeningById: getCurrentOpeningById,
    updateCurrentOpening: updateCurrentOpening,
    deleteCurrentOpening: deleteCurrentOpening
}
