/********
 * whatwedo.js file (services/whatwedo)
 ********/

const WhatWeDoData = require('../../models/whatwedo');
const fs = require("fs");

const addWhatWeDo = async (req, res, next) => {
    try {

        const {
            title,
            description,
            subtitle,
            status,
        } = req.body;

        if (title === undefined || title === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'title is required',
                'field': 'title'
            });
        }

        if (description === undefined || description === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'description is required',
                'field': 'description'
            });
        }

        if (subtitle === undefined || subtitle === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'subtitle is required',
                'field': 'subtitle'
            });
        }

        if (status === undefined || status === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'status is required',
                'field': 'status'
            });
        }

        if(req.file === undefined || req.file === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'image is required',
                'field': 'image'
            });
        }
        let image = req.file.path;
        const temp = {
            title: title,
            description: description,
            subtitle: subtitle,
            status: status,
            file: image
        }

        let testimonial = await new WhatWeDoData(temp);

        testimonial.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getWhatWeDo = async (req, res, next) => {
    try {

        let testimonial = await WhatWeDoData.find({});

        if (testimonial.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': testimonial
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getWhatWeDoById = async (req, res, next) => {
    try {
        let testimonial = await WhatWeDoData.findById(req.params.id);
        if (testimonial) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': testimonial
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateWhatWeDo = async (req, res, next) => {
    try {


        const TestimonialId = req.params.id;

        const {
            title,
            description,
            subtitle,
            status,
        } = req.body;

        if (TestimonialId === undefined || TestimonialId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'TestimonialId is required',
                'field': 'TestimonialId'
            });
        }

        let update_Testimonial_value = {};

        if (title !== undefined && title !== ''){
            update_Testimonial_value.title = title;
        }
        if (description !== undefined && description !== ''){
            update_Testimonial_value.description = description;
        }
        if (subtitle !== undefined && subtitle !== ''){
            update_Testimonial_value.subtitle = subtitle;
        }
        if (status !== undefined && status !== ''){
            update_Testimonial_value.status = status;
        }
        if (req.file !== undefined){
            update_Testimonial_value.file = req.file.path;
        }

        let Testimonialquery = { _id: TestimonialId };
        // when true return the updated document
        let options = {new:true};

        let Testimonial_value = { $set: update_Testimonial_value };


        let isDataExists = await WhatWeDoData.findById(TestimonialId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const TestimonialPromise = WhatWeDoData.findOneAndUpdate(Testimonialquery, Testimonial_value, options);

        Promise.all([TestimonialPromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteWhatWeDo = async (req, res, next) => {
    try {
        let testimonial = await WhatWeDoData.findByIdAndRemove(req.params.id);
        if (testimonial) {
            fs.unlink(testimonial.file, () =>{});

            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addWhatWeDo: addWhatWeDo,
    getWhatWeDo: getWhatWeDo,
    getWhatWeDoById: getWhatWeDoById,
    updateWhatWeDo: updateWhatWeDo,
    deleteWhatWeDo: deleteWhatWeDo
}
