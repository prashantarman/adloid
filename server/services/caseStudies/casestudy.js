/********
 * casestudy.js file (services/caseStudies)
 ********/

const CaseStudies = require('../../models/casestudy');

const addCaseStudy = async (req, res, next) => {
    try {

        const {
            title,
            details
        } = req.body;

        if (title === undefined || title === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'title is required',
                'field': 'title'
            });
        }

        if (details === undefined || details === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'details is required',
                'field': 'details'
            });
        }

        const temp = {
            title: title,
            details: details
        }

        let caseStudy = await new CaseStudies(temp);

        caseStudy.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getCaseStudy = async (req, res, next) => {
    try {

        let casestudy = await CaseStudies.find({});

        if (casestudy.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': casestudy
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getCaseStudyById = async (req, res, next) => {
    try {
        let casestudy = await CaseStudies.findById(req.params.id);
        if (casestudy) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': casestudy
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateCaseStudy = async (req, res, next) => {
    try {
        const caseId = req.params.id;

        const {
            title,
            details
        } = req.body;

        if (caseId === undefined || caseId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'caseId is required',
                'field': 'caseId'
            });
        }

        let update_casestudy_value = {};

        if (title !== undefined && title !== ''){
            update_casestudy_value.title = title;
        }
        if (details !== undefined && details !== ''){
            update_casestudy_value.details = details;
        }

        let casequery = { _id: caseId };
        // when true return the updated document
        let options = {new:true};

        let case_value = { $set: update_casestudy_value };


        let isDataExists = await CaseStudies.findById(caseId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const casePromise = CaseStudies.findOneAndUpdate(casequery, case_value, options);

        Promise.all([casePromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteCaseStudy = async (req, res, next) => {
    try {
        let casestudy = await CaseStudies.findByIdAndRemove(req.params.id);
        if (casestudy) {
            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `case with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addCaseStudy: addCaseStudy,
    getCaseStudy: getCaseStudy,
    getCaseStudyById: getCaseStudyById,
    updateCaseStudy: updateCaseStudy,
    deleteCaseStudy: deleteCaseStudy
}
