/********
 * contact.js file (services/contacts)
 ********/

const Contact = require('../../models/contact');

const addContact = async (req, res, next) => {
    try {

        const {
            icon,
            title,
            details
        } = req.body;

        if (icon === undefined || icon === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'icon is required',
                'field': 'icon'
            });
        }

        if (title === undefined || title === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'title is required',
                'field': 'title'
            });
        }

        if (details === undefined || details === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'details is required',
                'field': 'details'
            });
        }

        const temp = {
            icon: icon,
            title: title,
            details: details
        }

        let user = await new Contact(temp);

        user.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getContacts = async (req, res, next) => {
    try {

        let contacts = await Contact.find({});

        if (contacts.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': contacts
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getContactById = async (req, res, next) => {
    try {
        let contact = await Contact.findById(req.params.id);
        if (contact) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': contact
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateContact = async (req, res, next) => {
    try {


        const contactId = req.params.id;

        const {
            icon,
            title,
            details
        } = req.body;

        if (contactId === undefined || contactId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'contactId is required',
                'field': 'contactId'
            });
        }

        let update_contact_value = {};

        if (icon !== undefined && icon !== ''){
            update_contact_value.icon = icon;
        }
        if (title !== undefined && title !== ''){
            update_contact_value.title = title;
        }
        if (details !== undefined && details !== ''){
            update_contact_value.details = details;
        }

        let contactquery = { _id: contactId };
        // when true return the updated document
        let options = {new:true};

        let contact_value = { $set: update_contact_value };


        let isUserExists = await Contact.findById(contactId);

        if (!isUserExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No user found in the system'
            });
        }

        const contactPromise = Contact.findOneAndUpdate(contactquery, contact_value, options);

        Promise.all([contactPromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteContact = async (req, res, next) => {
    try {
        let contact = await Contact.findByIdAndRemove(req.params.id);
        if (contact) {
            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `contact with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addContact: addContact,
    getContacts: getContacts,
    getContactById: getContactById,
    deleteContact: deleteContact,
    updateContact: updateContact
}
