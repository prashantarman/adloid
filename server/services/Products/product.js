/********
 * product.js file (services/Products)
 ********/

const ProductData = require('../../models/products');

const addProduct = async (req, res, next) => {
    try {

        const {
            title,
            description,
            icon,
            iconTitle,
            iconDescription,
            icon1,
            iconTitle1,
            iconDescription1,
            icon2,
            iconTitle2,
            iconDescription2,
            lastTitle,
            lastDescription,
            lastTitle1,
            lastDescription1,
            lastTitle2,
            lastDescription2,
            bgColor
        } = req.body;

        if (title === undefined || title === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'title is required',
                'field': 'title'
            });
        }

        if (description === undefined || description === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'description is required',
                'field': 'description'
            });
        }
        if (icon === undefined || icon === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'icon is required',
                'field': 'icon'
            });
        }
        if (iconTitle === undefined || iconTitle === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconTitle is required',
                'field': 'iconTitle'
            });
        }
        if (iconDescription === undefined || iconDescription === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconDescription is required',
                'field': 'iconDescription'
            });
        }
        if (icon1 === undefined || icon1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'icon1 is required',
                'field': 'icon1'
            });
        }
        if (iconTitle1 === undefined || iconTitle1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconTitle1 is required',
                'field': 'iconTitle1'
            });
        }
        if (iconDescription1 === undefined || iconDescription1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconDescription1 is required',
                'field': 'iconDescription1'
            });
        }
        if (icon2 === undefined || icon2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'icon2 is required',
                'field': 'icon2'
            });
        }
        if (iconTitle2 === undefined || iconTitle2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconTitle2 is required',
                'field': 'iconTitle2'
            });
        }
        if (iconDescription2 === undefined || iconDescription2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconDescription2 is required',
                'field': 'iconDescription2'
            });
        }
        if (lastTitle === undefined || lastTitle === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastTitle is required',
                'field': 'lastTitle'
            });
        }
        if (lastDescription === undefined || lastDescription === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastDescription is required',
                'field': 'lastDescription'
            });
        }
        if (lastTitle1 === undefined || lastTitle1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastTitle1 is required',
                'field': 'lastTitle1'
            });
        }
        if (lastDescription1 === undefined || lastDescription1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastDescription1 is required',
                'field': 'lastDescription1'
            });
        }
        if (lastTitle2 === undefined || lastTitle2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastTitle2 is required',
                'field': 'lastTitle2'
            });
        }
        if (lastDescription2 === undefined || lastDescription2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastDescription2 is required',
                'field': 'lastDescription2'
            });
        }
        if (bgColor === undefined || bgColor === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'bgColor is required',
                'field': 'bgColor'
            });
        }
        if(req.file === undefined || req.file === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'file is required',
                'field': 'file'
            });
        }


        let image = req.file.path;

        const temp = {
            title: title,
            description: description,
            icon: icon,
            iconTitle: iconTitle,
            iconDescription: iconDescription,
            icon1: icon1,
            iconTitle1: iconTitle1,
            iconDescription1: iconDescription1,
            icon2: icon2,
            iconTitle2: iconTitle2,
            iconDescription2: iconDescription2,
            lastTitle: lastTitle,
            lastDescription: lastDescription,
            lastTitle1: lastTitle1,
            lastDescription1: lastDescription1,
            lastTitle2: lastTitle2,
            lastDescription2: lastDescription2,
            bgColor: bgColor,
            file: image
        }

        let Product = await new ProductData(temp);

        Product.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getProduct = async (req, res, next) => {
    try {

        let Product = await ProductData.find({});

        if (Product.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': Product
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getProductById = async (req, res, next) => {
    try {
        let Product = await ProductData.findById(req.params.id);
        if (Product) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': Product
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateProduct = async (req, res, next) => {
    try {
        const ProductId = req.params.id;
        
        const {
            title,
            description,
            icon,
            iconTitle,
            iconDescription,
            icon1,
            iconTitle1,
            iconDescription1,
            icon2,
            iconTitle2,
            iconDescription2,
            lastTitle,
            lastDescription,
            lastTitle1,
            lastDescription1,
            lastTitle2,
            lastDescription2,
            bgColor
        } = req.body;

        if (ProductId === undefined || ProductId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'ProductId is required',
                'field': 'ProductId'
            });
        }

        let update_Product_value = {};

        if (title !== undefined && title !== ''){
            update_Product_value.title = title;
        }
        if (description !== undefined && description !== ''){
            update_Product_value.description = description;
        }
        if (req.file !== undefined){
            update_Product_value.file = req.file.path;
        }
        if (icon !== undefined && icon !== ''){
            update_Product_value.icon = icon;
        }
        if (iconTitle !== undefined && iconTitle !== ''){
            update_Product_value.iconTitle = iconTitle;
        }
        if (iconDescription !== undefined && iconDescription !== ''){
            update_Product_value.iconDescription = iconDescription;
        }
        if (icon1 !== undefined && icon1 !== ''){
            update_Product_value.icon1 = icon1;
        }
        if (iconTitle1 !== undefined && iconTitle1 !== ''){
            update_Product_value.iconTitle1 = iconTitle1;
        }
        if (iconDescription1 !== undefined && iconDescription1 !== ''){
            update_Product_value.iconDescription1 = iconDescription1;
        }
        if (icon2 !== undefined && icon2 !== ''){
            update_Product_value.icon2 = icon2;
        }
        if (iconTitle2 !== undefined && iconTitle2 !== ''){
            update_Product_value.iconTitle2 = iconTitle2;
        }
        if (iconDescription2 !== undefined && iconDescription2 !== ''){
            update_Product_value.iconDescription2 = iconDescription2;
        }
        if (lastTitle !== undefined && lastTitle !== ''){
            update_Product_value.lastTitle = lastTitle;
        }
        if (lastDescription !== undefined && lastDescription !== ''){
            update_Product_value.lastDescription = lastDescription;
        }
        if (lastTitle1 !== undefined && lastTitle1 !== ''){
            update_Product_value.lastTitle1 = lastTitle1;
        }
        if (lastDescription1 !== undefined && lastDescription1 !== ''){
            update_Product_value.lastDescription1 = lastDescription1;
        }
        if (lastTitle2 !== undefined && lastTitle2 !== ''){
            update_Product_value.lastTitle2 = lastTitle2;
        }
        if (lastDescription2 !== undefined && lastDescription2 !== ''){
            update_Product_value.lastDescription2 = lastDescription2;
        }
        

        let casequery = { _id: ProductId };
        // when true return the updated document
        let options = {new:true};

        let case_value = { $set: update_Product_value };


        let isDataExists = await ProductData.findById(ProductId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const casePromise = ProductData.findOneAndUpdate(casequery, case_value, options);

        Promise.all([casePromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteProduct = async (req, res, next) => {
    try {
        let Product = await ProductData.findByIdAndRemove(req.params.id);
        if (Product) {
            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `case with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addProduct: addProduct,
    getProduct: getProduct,
    getProductById: getProductById,
    updateProduct: updateProduct,
    deleteProduct: deleteProduct
}
