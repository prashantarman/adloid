/********
 * ourProduct.js file (services/ourProducts)
 ********/

const OurProduct = require('../../models/ourProduct');
const fs = require("fs");

const addourProduct = async (req, res, next) => {
    try {

        const {
            name,
            description,
            subheading,
            link,
            status,
        } = req.body;

        if (name === undefined || name === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'name is required',
                'field': 'name'
            });
        }

        if (description === undefined || description === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'description is required',
                'field': 'description'
            });
        }

        if (subheading === undefined || subheading === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'subheading is required',
                'field': 'subheading'
            });
        }

        if (link === undefined || link === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'link is required',
                'field': 'link'
            });
        }

        if (status === undefined || status === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'status is required',
                'field': 'status'
            });
        }

        if(req.file === undefined || req.file === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'image is required',
                'field': 'image'
            });
        }
        let image = req.file.path;
        const temp = {
            name: name,
            description: description,
            subheading: subheading,
            link: link,
            status: status,
            file: image
        }

        let ourProduct = await new OurProduct(temp);

        ourProduct.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getourProduct = async (req, res, next) => {
    try {

        let ourProduct = await OurProduct.find({});

        if (ourProduct.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': ourProduct
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getourProductById = async (req, res, next) => {
    try {
        let ourProduct = await OurProduct.findById(req.params.id);
        if (ourProduct) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': ourProduct
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateourProduct = async (req, res, next) => {
    try {


        const ourProductId = req.params.id;

        const {
            name,
            description,
            subheading,
            link,
            status,
        } = req.body;

        if (ourProductId === undefined || ourProductId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'ourProductId is required',
                'field': 'ourProductId'
            });
        }

        let update_ourProduct_value = {};

        if (name !== undefined && name !== ''){
            update_ourProduct_value.name = name;
        }
        if (description !== undefined && description !== ''){
            update_ourProduct_value.description = description;
        }
        if (subheading !== undefined && subheading !== ''){
            update_ourProduct_value.subheading = subheading;
        }
        if (link !== undefined && link !== ''){
            update_ourProduct_value.link = link;
        }
        if (status !== undefined && status !== ''){
            update_ourProduct_value.status = status;
        }
        if (req.file !== undefined){
            update_ourProduct_value.file = req.file.path;
        }

        let ourProductquery = { _id: ourProductId };
        // when true return the updated document
        let options = {new:true};

        let ourProduct_value = { $set: update_ourProduct_value };


        let isDataExists = await OurProduct.findById(ourProductId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const ourProductPromise = OurProduct.findOneAndUpdate(ourProductquery, ourProduct_value, options);

        Promise.all([ourProductPromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteourProduct = async (req, res, next) => {
    try {
        let ourProduct = await OurProduct.findByIdAndRemove(req.params.id);
        if (ourProduct) {
            fs.unlink(ourProduct.file, () =>{});

            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addourProduct: addourProduct,
    getourProduct: getourProduct,
    getourProductById: getourProductById,
    updateourProduct: updateourProduct,
    deleteourProduct: deleteourProduct
}
