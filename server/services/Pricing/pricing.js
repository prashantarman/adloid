/********
 * Pricing.js file (services/Pricing)
 ********/

const PricingData = require('../../models/pricing');

const addPricing = async (req, res, next) => {
    try {

        const {
            title,
            subtitle,
            price,
            prodconf,
            impressionyear,
            assetsmanagement,
            augmentedreality
        } = req.body;

        if (title === undefined || title === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'title is required',
                'field': 'title'
            });
        }

        if (subtitle === undefined || subtitle === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'subtitle is required',
                'field': 'subtitle'
            });
        }
        if (price === undefined || price === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'price is required',
                'field': 'price'
            });
        }
        if (prodconf === undefined || prodconf === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'prodconf is required',
                'field': 'prodconf'
            });
        }
        if (impressionyear === undefined || impressionyear === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'impressionyear is required',
                'field': 'impressionyear'
            });
        }
        if (assetsmanagement === undefined || assetsmanagement === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'assetsmanagement is required',
                'field': 'assetsmanagement'
            });
        }
        if (augmentedreality === undefined || augmentedreality === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'augmentedreality is required',
                'field': 'augmentedreality'
            });
        }

        const temp = {
            title: title,
            subtitle: subtitle,
            price: price,
            prodconf: prodconf,
            impressionyear: impressionyear,
            assetsmanagement: assetsmanagement,
            augmentedreality: augmentedreality,
        }

        let Pricing = await new PricingData(temp);

        Pricing.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getPricing = async (req, res, next) => {
    try {

        let Pricing = await PricingData.find({});

        if (Pricing.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': Pricing
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getPricingById = async (req, res, next) => {
    try {
        let Pricing = await PricingData.findById(req.params.id);
        if (Pricing) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': Pricing
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updatePricing = async (req, res, next) => {
    try {
        const pricingId = req.params.id;
        
        const {
            title,
            subtitle,
            price,
            prodconf,
            impressionyear,
            assetsmanagement,
            augmentedreality
        } = req.body;

        if (pricingId === undefined || pricingId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'pricingId is required',
                'field': 'pricingId'
            });
        }

        let update_Pricing_value = {};

        if (title !== undefined && title !== ''){
            update_Pricing_value.title = title;
        }
        if (subtitle !== undefined && subtitle !== ''){
            update_Pricing_value.subtitle = subtitle;
        }
        if (price !== undefined && price !== ''){
            update_Pricing_value.price = price;
        }
        if (prodconf !== undefined && prodconf !== ''){
            update_Pricing_value.prodconf = prodconf;
        }
        if (impressionyear !== undefined && impressionyear !== ''){
            update_Pricing_value.impressionyear = impressionyear;
        }
        if (assetsmanagement !== undefined && assetsmanagement !== ''){
            update_Pricing_value.assetsmanagement = assetsmanagement;
        }
        if (augmentedreality !== undefined && augmentedreality !== ''){
            update_Pricing_value.augmentedreality = augmentedreality;
        }
        

        let casequery = { _id: pricingId };
        // when true return the updated document
        let options = {new:true};

        let case_value = { $set: update_Pricing_value };


        let isDataExists = await PricingData.findById(pricingId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const casePromise = PricingData.findOneAndUpdate(casequery, case_value, options);

        Promise.all([casePromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deletePricing = async (req, res, next) => {
    try {
        let Pricing = await PricingData.findByIdAndRemove(req.params.id);
        if (Pricing) {
            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `case with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addPricing: addPricing,
    getPricing: getPricing,
    getPricingById: getPricingById,
    updatePricing: updatePricing,
    deletePricing: deletePricing
}
