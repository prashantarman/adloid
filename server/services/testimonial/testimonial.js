/********
 * Testimonial.js file (services/Testimonial)
 ********/

const Testimonial = require('../../models/testimonial');
const fs = require("fs");

const addTestimonial = async (req, res, next) => {
    try {

        const {
            name,
            description,
            designation,
            status,
        } = req.body;

        if (name === undefined || name === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'name is required',
                'field': 'name'
            });
        }

        if (description === undefined || description === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'description is required',
                'field': 'description'
            });
        }

        if (designation === undefined || designation === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'designation is required',
                'field': 'designation'
            });
        }

        if (status === undefined || status === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'status is required',
                'field': 'status'
            });
        }

        if(req.file === undefined || req.file === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'image is required',
                'field': 'image'
            });
        }
        let image = req.file.path;
        const temp = {
            name: name,
            description: description,
            designation: designation,
            status: status,
            file: image
        }

        let testimonial = await new Testimonial(temp);

        testimonial.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getTestimonial = async (req, res, next) => {
    try {

        let testimonial = await Testimonial.find({});

        if (testimonial.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': testimonial
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getTestimonialById = async (req, res, next) => {
    try {
        let testimonial = await Testimonial.findById(req.params.id);
        if (testimonial) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': testimonial
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateTestimonial = async (req, res, next) => {
    try {


        const TestimonialId = req.params.id;

        const {
            name,
            description,
            designation,
            status,
        } = req.body;

        if (TestimonialId === undefined || TestimonialId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'TestimonialId is required',
                'field': 'TestimonialId'
            });
        }

        let update_Testimonial_value = {};

        if (name !== undefined && name !== ''){
            update_Testimonial_value.name = name;
        }
        if (description !== undefined && description !== ''){
            update_Testimonial_value.description = description;
        }
        if (designation !== undefined && designation !== ''){
            update_Testimonial_value.designation = designation;
        }
        if (status !== undefined && status !== ''){
            update_Testimonial_value.status = status;
        }
        if (req.file !== undefined){
            update_Testimonial_value.file = req.file.path;
        }

        let Testimonialquery = { _id: TestimonialId };
        // when true return the updated document
        let options = {new:true};

        let Testimonial_value = { $set: update_Testimonial_value };


        let isDataExists = await Testimonial.findById(TestimonialId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const TestimonialPromise = Testimonial.findOneAndUpdate(Testimonialquery, Testimonial_value, options);

        Promise.all([TestimonialPromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteTestimonial = async (req, res, next) => {
    try {
        let testimonial = await Testimonial.findByIdAndRemove(req.params.id);
        if (testimonial) {
            fs.unlink(testimonial.file, () =>{});

            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addTestimonial: addTestimonial,
    getTestimonial: getTestimonial,
    getTestimonialById: getTestimonialById,
    updateTestimonial: updateTestimonial,
    deleteTestimonial: deleteTestimonial
}
