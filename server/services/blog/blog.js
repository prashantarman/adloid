/********
 * blog.js file (services/blog)
 ********/

const Blog = require('../../models/blog');
const fs = require("fs");

const addBlog = async (req, res, next) => {
    try {

        const {
            title,
            description,
            updateddate,
            updatedby,
            status,
        } = req.body;

        if (title === undefined || title === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'title is required',
                'field': 'title'
            });
        }

        if (description === undefined || description === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'description is required',
                'field': 'description'
            });
        }

        if (updateddate === undefined || updateddate === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'updateddate is required',
                'field': 'updateddate'
            });
        }

        if (updatedby === undefined || updatedby === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'updatedby is required',
                'field': 'updatedby'
            });
        }

        if (status === undefined || status === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'status is required',
                'field': 'status'
            });
        }

        if(req.file === undefined || req.file === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'image is required',
                'field': 'image'
            });
        }
        let image = req.file.path;
        const temp = {
            title: title,
            description: description,
            updateddate: updateddate,
            updatedby: updatedby,
            status: status,
            file: image
        }

        let blog = await new Blog(temp);

        blog.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getBlog = async (req, res, next) => {
    try {

        let blog = await Blog.find({});

        if (blog.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': blog
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getBlogById = async (req, res, next) => {
    try {
        let blog = await Blog.findById(req.params.id);
        if (blog) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': blog
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateBlog = async (req, res, next) => {
    try {


        const BlogId = req.params.id;

        const {
            title,
            description,
            updateddate,
            updatedby,
            status,
        } = req.body;

        if (BlogId === undefined || BlogId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'BlogId is required',
                'field': 'BlogId'
            });
        }

        let update_Blog_value = {};

        if (title !== undefined && title !== ''){
            update_Blog_value.title = title;
        }
        if (description !== undefined && description !== ''){
            update_Blog_value.description = description;
        }
        if (updateddate !== undefined && updateddate !== ''){
            update_Blog_value.updateddate = updateddate;
        }
        if (updatedby !== undefined && updatedby !== ''){
            update_Blog_value.updatedby = updatedby;
        }
        if (status !== undefined && status !== ''){
            update_Blog_value.status = status;
        }
        if (req.file !== undefined){
            update_Blog_value.file = req.file.path;
        }

        let Blogquery = { _id: BlogId };
        // when true return the updated document
        let options = {new:true};

        let Blog_value = { $set: update_Blog_value };


        let isDataExists = await Blog.findById(BlogId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const BlogPromise = Blog.findOneAndUpdate(Blogquery, Blog_value, options);

        Promise.all([BlogPromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteBlog = async (req, res, next) => {
    try {
        let blog = await Blog.findByIdAndRemove(req.params.id);
        if (blog) {
            fs.unlink(blog.file, () =>{});

            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addBlog: addBlog,
    getBlog: getBlog,
    getBlogById: getBlogById,
    updateBlog: updateBlog,
    deleteBlog: deleteBlog
}
