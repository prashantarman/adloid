/********
 * arsolution.js file (services/ArSolutions)
 ********/

const ArSolutionData = require('../../models/arsolutions');

const addArSolution = async (req, res, next) => {
    try {

        const {
            icon,
            iconTitle,
            iconDescription,
            icon1,
            iconTitle1,
            iconDescription1,
            icon2,
            iconTitle2,
            iconDescription2,
            lastTitle,
            lastDescription,
            lastTitle1,
            lastDescription1,
            lastTitle2,
            lastDescription2,
            status
        } = req.body;


        if (icon === undefined || icon === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'icon is required',
                'field': 'icon'
            });
        }
        if (iconTitle === undefined || iconTitle === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconTitle is required',
                'field': 'iconTitle'
            });
        }
        if (iconDescription === undefined || iconDescription === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconDescription is required',
                'field': 'iconDescription'
            });
        }
        if (icon1 === undefined || icon1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'icon1 is required',
                'field': 'icon1'
            });
        }
        if (iconTitle1 === undefined || iconTitle1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconTitle1 is required',
                'field': 'iconTitle1'
            });
        }
        if (iconDescription1 === undefined || iconDescription1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconDescription1 is required',
                'field': 'iconDescription1'
            });
        }
        if (icon2 === undefined || icon2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'icon2 is required',
                'field': 'icon2'
            });
        }
        if (iconTitle2 === undefined || iconTitle2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconTitle2 is required',
                'field': 'iconTitle2'
            });
        }
        if (iconDescription2 === undefined || iconDescription2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'iconDescription2 is required',
                'field': 'iconDescription2'
            });
        }
        if (lastTitle === undefined || lastTitle === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastTitle is required',
                'field': 'lastTitle'
            });
        }
        if (lastDescription === undefined || lastDescription === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastDescription is required',
                'field': 'lastDescription'
            });
        }
        if (lastTitle1 === undefined || lastTitle1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastTitle1 is required',
                'field': 'lastTitle1'
            });
        }
        if (lastDescription1 === undefined || lastDescription1 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastDescription1 is required',
                'field': 'lastDescription1'
            });
        }
        if (lastTitle2 === undefined || lastTitle2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastTitle2 is required',
                'field': 'lastTitle2'
            });
        }
        if (lastDescription2 === undefined || lastDescription2 === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'lastDescription2 is required',
                'field': 'lastDescription2'
            });
        }
        if (status === undefined || status === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'status is required',
                'field': 'status'
            });
        }

        const temp = {
            icon: icon,
            iconTitle: iconTitle,
            iconDescription: iconDescription,
            icon1: icon1,
            iconTitle1: iconTitle1,
            iconDescription1: iconDescription1,
            icon2: icon2,
            iconTitle2: iconTitle2,
            iconDescription2: iconDescription2,
            lastTitle: lastTitle,
            lastDescription: lastDescription,
            lastTitle1: lastTitle1,
            lastDescription1: lastDescription1,
            lastTitle2: lastTitle2,
            lastDescription2: lastDescription2,
            status: (status)?status:true,
        }

        let ArSolution = await new ArSolutionData(temp);

        ArSolution.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getArSolution = async (req, res, next) => {
    try {

        let ArSolution = await ArSolutionData.find({});

        if (ArSolution.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': ArSolution
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getArSolutionById = async (req, res, next) => {
    try {
        let ArSolution = await ArSolutionData.findById(req.params.id);
        if (ArSolution) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': ArSolution
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateArSolution = async (req, res, next) => {
    try {
        const ArSolutionId = req.params.id;
        
        const {
            icon,
            iconTitle,
            iconDescription,
            icon1,
            iconTitle1,
            iconDescription1,
            icon2,
            iconTitle2,
            iconDescription2,
            lastTitle,
            lastDescription,
            lastTitle1,
            lastDescription1,
            lastTitle2,
            lastDescription2,
            status
        } = req.body;

        if (ArSolutionId === undefined || ArSolutionId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'ArSolutionId is required',
                'field': 'ArSolutionId'
            });
        }

        let update_ArSolution_value = {};

        if (icon !== undefined && icon !== ''){
            update_ArSolution_value.icon = icon;
        }
        if (iconTitle !== undefined && iconTitle !== ''){
            update_ArSolution_value.iconTitle = iconTitle;
        }
        if (iconDescription !== undefined && iconDescription !== ''){
            update_ArSolution_value.iconDescription = iconDescription;
        }
        if (icon1 !== undefined && icon1 !== ''){
            update_ArSolution_value.icon1 = icon1;
        }
        if (iconTitle1 !== undefined && iconTitle1 !== ''){
            update_ArSolution_value.iconTitle1 = iconTitle1;
        }
        if (iconDescription1 !== undefined && iconDescription1 !== ''){
            update_ArSolution_value.iconDescription1 = iconDescription1;
        }
        if (icon2 !== undefined && icon2 !== ''){
            update_ArSolution_value.icon2 = icon2;
        }
        if (iconTitle2 !== undefined && iconTitle2 !== ''){
            update_ArSolution_value.iconTitle2 = iconTitle2;
        }
        if (iconDescription2 !== undefined && iconDescription2 !== ''){
            update_ArSolution_value.iconDescription2 = iconDescription2;
        }
        if (lastTitle !== undefined && lastTitle !== ''){
            update_ArSolution_value.lastTitle = lastTitle;
        }
        if (lastDescription !== undefined && lastDescription !== ''){
            update_ArSolution_value.lastDescription = lastDescription;
        }
        if (lastTitle1 !== undefined && lastTitle1 !== ''){
            update_ArSolution_value.lastTitle1 = lastTitle1;
        }
        if (lastDescription1 !== undefined && lastDescription1 !== ''){
            update_ArSolution_value.lastDescription1 = lastDescription1;
        }
        if (lastTitle2 !== undefined && lastTitle2 !== ''){
            update_ArSolution_value.lastTitle2 = lastTitle2;
        }
        if (lastDescription2 !== undefined && lastDescription2 !== ''){
            update_ArSolution_value.lastDescription2 = lastDescription2;
        }
        if (status !== undefined && status !== ''){
            update_ArSolution_value.status = status;
        }
        

        let casequery = { _id: ArSolutionId };
        // when true return the updated document
        let options = {new:true};

        let case_value = { $set: update_ArSolution_value };


        let isDataExists = await ArSolutionData.findById(ArSolutionId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const casePromise = ArSolutionData.findOneAndUpdate(casequery, case_value, options);

        Promise.all([casePromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteArSolution = async (req, res, next) => {
    try {
        let ArSolution = await ArSolutionData.findByIdAndRemove(req.params.id);
        if (ArSolution) {
            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `case with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addArSolution: addArSolution,
    getArSolution: getArSolution,
    getArSolutionById: getArSolutionById,
    updateArSolution: updateArSolution,
    deleteArSolution: deleteArSolution
}
