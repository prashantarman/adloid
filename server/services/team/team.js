/********
 * team.js file (services/clients)
 ********/

const Team = require('../../models/team');
const fs = require("fs");

const addTeam = async (req, res, next) => {
    try {

        const {
            name,
            designation,
            description,
            linkedin
        } = req.body;

        if (name === undefined || name === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'name is required',
                'field': 'name'
            });
        }

        if (designation === undefined || designation === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'designation is required',
                'field': 'designation'
            });
        }

        if (description === undefined || description === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'description is required',
                'field': 'description'
            });
        }

        if (linkedin === undefined || linkedin === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'linkedin is required',
                'field': 'linkedin'
            });
        }

        if(req.file === undefined || req.file === ''){
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'image is required',
                'field': 'image'
            });
        }
        let image = req.file.path;
        const temp = {
            name: name,
            designation: designation,
            description: description,
            linkedin: linkedin,
            file: image
        }

        let client = await new Team(temp);

        client.save()
            .then((newData) => {
                if (newData) {
                    return res.status(201).json({
                        'code': 'SUCCESS',
                        'message': 'Data Added successfully',
                        'data': newData
                    });
                } else {
                    return res.status(500).json({
                        'code': 'SERVER_ERROR',
                        'message': 'something went wrong'
                    });
                }
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getTeam = async (req, res, next) => {
    try {

        let client = await Team.find({});

        if (client.length > 0) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': 'Data fetched successfully',
                'data': client
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const getTeamById = async (req, res, next) => {
    try {
        let client = await Team.findById(req.params.id);
        if (client) {
            return res.status(200).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} fetched successfully`,
                'data': client
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No data found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const updateTeam = async (req, res, next) => {
    try {


        const clientId = req.params.id;

        const {
            name,
            designation,
            description,
            linkedin
        } = req.body;

        if (clientId === undefined || clientId === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'message': 'ID is required',
                'field': 'ID'
            });
        }

        let update_client_value = {};

        if (name !== undefined && name !== ''){
            update_client_value.name = name;
        }
        if (designation !== undefined && designation !== ''){
            update_client_value.designation = designation;
        }
        if (description !== undefined && description !== ''){
            update_client_value.description = description;
        }
        if (linkedin !== undefined && linkedin !== ''){
            update_client_value.linkedin = linkedin;
        }
        if (req.file !== undefined){
            update_client_value.file = req.file.path;
        }

        let clientquery = { _id: clientId };
        // when true return the updated document
        let options = {new:true};

        let client_value = { $set: update_client_value };


        let isDataExists = await Team.findById(clientId);

        if (!isDataExists) {
            return res.status(404).json({
                'code': 'BAD_REQUEST_ERROR',
                'message': 'No data found in the system'
            });
        }

        const clientPromise = Team.findOneAndUpdate(clientquery, client_value, options);

        Promise.all([clientPromise])
            .then((result) => {
                if (result){
                    return res.status(200).json({
                        'code': 'SUCCESS',
                        'message': 'Data updated successfully',
                        'data': result
                    });
                }
                return res.status(400).json({
                    'code': 'SERVER_ERROR',
                    'message': 'something went wrong, Please try again'
                });
            })
            .catch((err) => {
                return res.status(500).json({
                    'code': 'SERVER_ERROR',
                    'message': err.message
                });
            });



    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

const deleteTeam = async (req, res, next) => {
    try {
        let client = await Team.findByIdAndRemove(req.params.id);
        if (client) {
            fs.unlink(client.file, () =>{});

            return res.status(202).json({
                'code': 'SUCCESS',
                'message': `data with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'message': 'No users found in the system'
        });

    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'message': 'something went wrong, Please try again'
        });
    }
};

module.exports = {
    addTeam: addTeam,
    getTeam: getTeam,
    getTeamById: getTeamById,
    updateTeam: updateTeam,
    deleteTeam: deleteTeam
}
