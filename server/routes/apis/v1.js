/********
* v1.js file (inside routes/apis)
********/
const express = require('express');
let router = express.Router();


const userController = require('../../controllers/apis/user');
const contactController = require('../../controllers/apis/contact');
const caseStudyController = require('../../controllers/apis/casestudy');
const clientController = require('../../controllers/apis/client');
const pricingController = require('../../controllers/apis/pricing');
const productController = require('../../controllers/apis/product');
const teamController = require('../../controllers/apis/team');
const whyJoinController = require('../../controllers/apis/whyjoin');
const careerController = require('../../controllers/apis/career');
const currentOpeningsController = require('../../controllers/apis/currentOpening');
const arsolutionController = require('../../controllers/apis/arsolution');
const blogController = require('../../controllers/apis/blog');
const testimonialController = require('../../controllers/apis/testimonial');
const ourProductController = require('../../controllers/apis/ourProducts');
const whatwedoController = require('../../controllers/apis/whatwedo');
const sliderController = require('../../controllers/apis/slider');

router.use('/users', userController);
router.use('/contacts', contactController);
router.use('/casestudy', caseStudyController);
router.use('/client', clientController);
router.use('/pricing', pricingController);
router.use('/product', productController);
router.use('/team', teamController);
router.use('/whyjoin', whyJoinController);
router.use('/careers', careerController);
router.use('/currentOpenings', currentOpeningsController);
router.use('/arsolutions', arsolutionController);
router.use('/blog', blogController);
router.use('/testimonial', testimonialController);
router.use('/ourProduct', ourProductController);
router.use('/whatwedo', whatwedoController);
router.use('/slider', sliderController);

module.exports = router;
