/********
* casestudy.js file (controllers/apis)
********/


const express = require('express');
const caseService = require('../../services/caseStudies/casestudy');
let router = express.Router();

router.get('/', caseService.getCaseStudy);

router.get('/:id', caseService.getCaseStudyById);

router.post('/', caseService.addCaseStudy);

router.put('/:id', caseService.updateCaseStudy);

router.delete('/:id', caseService.deleteCaseStudy);

module.exports = router;
