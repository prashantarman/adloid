/********
* ourProduct.js file (controllers/apis)
********/


const express = require('express');
const ourProductService = require('../../services/ourProducts/ourProduct');
let router = express.Router();
const multer = require('multer');
const mime = require('mime-types');

const randtoken = require('rand-token').suid;

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/products/');
    },
    filename: function(req, file, cb) {
        let token = randtoken(16);
        let ext;
        let allowed = ['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG', 'mp4'];
        let extension = mime.extension(file.mimetype);
        if (allowed.indexOf(extension) > -1) {
            ext = '.'+extension;
        } else {
            ext = '.png';
        }
        // console.log(ext);
        cb(null, new Date().toISOString() + token+ext);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png', file.mimetype === 'video/mp4') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 10
    },
    fileFilter: fileFilter
});

router.get('/', ourProductService.getourProduct);

router.get('/:id', ourProductService.getourProductById);

router.post('/', upload.single('image'),ourProductService.addourProduct);

router.put('/:id', upload.single('image'),ourProductService.updateourProduct);

router.delete('/:id', ourProductService.deleteourProduct);

module.exports = router;
