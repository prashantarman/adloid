/********
* client.js file (controllers/apis)
********/


const express = require('express');
const clientService = require('../../services/clients/client');
let router = express.Router();
const multer = require('multer');
const mime = require('mime-types');
const path = require("path");

const randtoken = require('rand-token').suid;

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/clients/');
    },
    filename: function(req, file, cb) {
        let token = randtoken(16);
        let ext;
        let allowed = ['.png', '.PNG', '.jpg', '.JPG', '.jpeg', '.JPEG'];
        let extension = mime.extension(file.mimetype);
        if (allowed.indexOf(extension) > -1) {
            ext = extension;
        } else {
            ext = '.png';
        }
        // console.log(ext);
        cb(null, new Date().toISOString() + token+ext);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 10
    },
    fileFilter: fileFilter
});

router.get('/', clientService.getClientStudy);

router.get('/:id', clientService.getClientById);

router.post('/', upload.single('image'),clientService.addClient);

router.put('/:id', upload.single('image'),clientService.updateClient);

router.delete('/:id', clientService.deleteClient);

module.exports = router;
