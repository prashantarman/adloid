/********
* Slider.js file (controllers/apis)
********/


const express = require('express');
const sliderService = require('../../services/sliders/slider');
let router = express.Router();
const multer = require('multer');
const mime = require('mime-types');

const randtoken = require('rand-token').suid;

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/sliders/');
    },
    filename: function(req, file, cb) {
        let token = randtoken(16);
        let ext;
        let allowed = ['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG', 'mp4'];
        let extension = mime.extension(file.mimetype);
        if (allowed.indexOf(extension) > -1) {
            ext = '.'+extension;
        } else {
            ext = '.png';
        }
        // console.log(ext);
        cb(null, new Date().toISOString() + token+ext);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png', file.mimetype === 'video/mp4') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 500
    },
    fileFilter: fileFilter
});

router.get('/', sliderService.getSlider);

router.get('/:id', sliderService.getSliderById);

router.post('/', upload.single('image'),sliderService.addSlider);

router.put('/:id', upload.single('image'),sliderService.updateSlider);

router.delete('/:id', sliderService.deleteSlider);

module.exports = router;
