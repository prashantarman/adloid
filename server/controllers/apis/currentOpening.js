/********
* casestudy.js file (controllers/apis)
********/


const express = require('express');
const CurrentOpeningService = require('../../services/currentOpenings/currentOpening');
let router = express.Router();

router.get('/', CurrentOpeningService.getCurrentOpening);

router.get('/:id', CurrentOpeningService.getCurrentOpeningById);

router.post('/', CurrentOpeningService.addCurrentOpening);

router.put('/:id', CurrentOpeningService.updateCurrentOpening);

router.delete('/:id', CurrentOpeningService.deleteCurrentOpening);

module.exports = router;
