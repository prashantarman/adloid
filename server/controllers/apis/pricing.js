/********
* pricing.js file (controllers/apis)
********/


const express = require('express');
const pricingService = require('../../services/Pricing/pricing');
let router = express.Router();

router.get('/', pricingService.getPricing);

router.get('/:id', pricingService.getPricingById);

router.post('/', pricingService.addPricing);

router.put('/:id', pricingService.updatePricing);

router.delete('/:id', pricingService.deletePricing);

module.exports = router;
