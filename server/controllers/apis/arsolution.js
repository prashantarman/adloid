/********
* arsolution.js file (controllers/apis)
********/


const express = require('express');
const ArSolutionService = require('../../services/ArSolutions/arsolution');
let router = express.Router();


router.get('/', ArSolutionService.getArSolution);

router.get('/:id', ArSolutionService.getArSolutionById);

router.post('/', ArSolutionService.addArSolution);

router.put('/:id', ArSolutionService.updateArSolution);

router.delete('/:id', ArSolutionService.deleteArSolution);

module.exports = router;
