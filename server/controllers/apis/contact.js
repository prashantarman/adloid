/********
* contact.js file (controllers/apis)
********/


const express = require('express');
const contactService = require('../../services/contacts/contact');
let router = express.Router();

router.get('/', contactService.getContacts);

router.get('/:id', contactService.getContactById);

router.post('/', contactService.addContact);

router.put('/:id', contactService.updateContact);

router.delete('/:id', contactService.deleteContact);

module.exports = router;
