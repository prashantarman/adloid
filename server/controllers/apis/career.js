/********
* client.js file (controllers/apis)
********/


const express = require('express');
const careerService = require('../../services/careers/career');
let router = express.Router();
const multer = require('multer');
const mime = require('mime-types');

const randtoken = require('rand-token').suid;

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/career/');
    },
    filename: function(req, file, cb) {
        // console.log(req.files);
        let token = randtoken(16);
        let ext;
        let allowed = ['.png', '.PNG', '.jpg', '.JPG', '.jpeg', '.JPEG'];
        let extension = mime.extension(file.mimetype);
        if (allowed.indexOf(extension) > -1) {
            ext = extension;
        } else {
            ext = '.png';
        }
        // console.log(ext);
        cb(null, new Date().toISOString() + token+ext);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 10
    },
    fileFilter: fileFilter
});

router.get('/', careerService.getCareer);

router.get('/:id', careerService.getCareerById);

router.post('/', upload.fields([{name:'image', maxCount: 1},{name:'image1', maxCount: 1}]),careerService.addCareer);

router.put('/:id', upload.fields([{name:'image', maxCount: 1},{name:'image1', maxCount: 1}]),careerService.updateCareer);

router.delete('/:id', careerService.deleteCareer);

module.exports = router;
