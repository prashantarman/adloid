/********
* whyjoin.js file (controllers/apis)
********/


const express = require('express');
const whyJoinService = require('../../services/whyJoin/whyjoin');
let router = express.Router();

router.get('/', whyJoinService.getWhyJoin);

router.get('/:id', whyJoinService.getWhyJoinById);

router.post('/', whyJoinService.addWhyJoin);

router.put('/:id', whyJoinService.updateWhyJoin);

router.delete('/:id', whyJoinService.deleteWhyJoin);

module.exports = router;
