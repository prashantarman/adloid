/********
 * career.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Client = new Schema({
    title: {
        type: String,
        required : [ true, 'Title is required'],
    },
    description: {
        type: String,
        required : [ true, 'description is required'],
    },
    file: {
        type: String,
        required : [ true, 'Image is required'],
    },
    file1: {
        type: String,
        required : [ true, 'Image2 is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_career', Client);
