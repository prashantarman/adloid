/********
 * arsolutions.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Pricing = new Schema({

    icon: {
        type: String,
        required : [ true, 'Icon is required'],
    },
    iconTitle: {
        type: String,
        required : [ true, 'Icon Title is required'],
    },
    iconDescription: {
        type: String,
        required : [ true, 'Icon Description is required'],
    },
    icon1: {
        type: String,
        required : [ true, 'Icon1 is required'],
    },
    iconTitle1: {
        type: String,
        required : [ true, 'Icon1 Title is required'],
    },
    iconDescription1: {
        type: String,
        required : [ true, 'Icon1 Description is required'],
    },
    icon2: {
        type: String,
        required : [ true, 'Icon2 is required'],
    },
    iconTitle2: {
        type: String,
        required : [ true, 'Icon2 Title is required'],
    },
    iconDescription2: {
        type: String,
        required : [ true, 'Icon2 Description is required'],
    },
    lastTitle: {
        type: String,
        required : [ true, 'Last Title is required'],
    },
    lastDescription: {
        type: String,
        required : [ true, 'lastDescription is required'],
    },
    lastTitle1: {
        type: String,
        required : [ true, 'Last Title1 is required'],
    },
    lastDescription1: {
        type: String,
        required : [ true, 'lastDescription1 is required'],
    },
    lastTitle2: {
        type: String,
        required : [ true, 'Last Title2 is required'],
    },
    lastDescription2: {
        type: String,
        required : [ true, 'lastDescription2 is required'],
    },
    status: {
        type: Boolean,
        required : [ true, 'Bg Color is required'],
        default: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_arsolution', Pricing);
