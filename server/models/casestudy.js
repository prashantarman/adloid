/********
 * caseStudy.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const CaaseStudy = new Schema({
    title: {
        type: String,
        required : [ true, 'Title is required'],
    },
    details: {
        type: String,
        required : [ true, 'Details is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_casestudy', CaaseStudy);
