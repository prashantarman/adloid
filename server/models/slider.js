/********
 * slider.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Client = new Schema({
    link: {
        type: String,
        required : [ true, 'Title is required'],
    },
    file: {
        type: String,
        required : [ true, 'Image is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_slider', Client);
