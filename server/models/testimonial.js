/********
 * testimonial.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Client = new Schema({
    name: {
        type: String,
        required : [ true, 'Title is required'],
    },
    description: {
        type: String,
        required : [ true, 'description is required'],
    },
    designation: {
        type: String,
        required : [ true, 'updateddate is required'],
    },
    status: {
        type: Boolean,
        required : [ true, 'status is required'],
        default: true
    },
    file: {
        type: String,
        required : [ true, 'Image is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_testimonial', Client);
