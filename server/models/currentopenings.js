/********
 * currentOpenings.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const CaaseStudy = new Schema({
    jobid: {
        type: String,
        required : [ true, 'JobId is required'],
    },
    title: {
        type: String,
        required : [ true, 'Title is required'],
    },
    details: {
        type: String,
        required : [ true, 'Details is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_currentopenings', CaaseStudy);
