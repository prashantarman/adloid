/********
 * user.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Contact = new Schema({
    icon: {
        type: String,
        required : [ true, 'icon is required'],
        lowercase : true
    },
    title: {
        type: String,
        required : [ true, 'Title is required'],
    },
    details: {
        type: String,
        required : [ true, 'Details is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_contact', Contact);
