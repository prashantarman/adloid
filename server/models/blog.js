/********
 * blog.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Client = new Schema({
    title: {
        type: String,
        required : [ true, 'Title is required'],
    },
    description: {
        type: String,
        required : [ true, 'description is required'],
    },
    updateddate: {
        type: String,
        required : [ true, 'updateddate is required'],
    },
    updatedby: {
        type: String,
        required : [ true, 'updatedby is required'],
    },
    status: {
        type: Boolean,
        required : [ true, 'status is required'],
        default: true
    },
    file: {
        type: String,
        required : [ true, 'Image is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_blog', Client);
