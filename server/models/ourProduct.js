/********
 * career.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Client = new Schema({
    name: {
        type: String,
        required : [ true, 'Title is required'],
    },
    subheading: {
        type: String,
        required : [ true, 'Title is required'],
    },
    description: {
        type: String,
        required : [ true, 'description is required'],
    },
    link: {
        type: String,
        required : [ true, 'Link is required'],
    },
    file: {
        type: String,
        required : [ true, 'Image is required'],
    },
    status: {
        type: Boolean,
        required : [ true, 'status is required'],
        default: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_ourproduct', Client);
