/********
 * whatwedo.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Client = new Schema({
    title: {
        type: String,
        required : [ true, 'title is required'],
    },
    description: {
        type: String,
        required : [ true, 'description is required'],
    },
    subtitle: {
        type: String,
        required : [ true, 'subtitle is required'],
    },
    status: {
        type: Boolean,
        required : [ true, 'status is required'],
        default: true
    },
    file: {
        type: String,
        required : [ true, 'Image is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_whatwedo', Client);
