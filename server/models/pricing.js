/********
 * pricing.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Pricing = new Schema({
    title: {
        type: String,
        required : [ true, 'Title is required'],
    },
    subtitle: {
        type: String,
        required : [ true, 'Sub Title is required'],
    },
    price: {
        type: String,
        required : [ true, 'Price is required'],
    },
    prodconf: {
        type: String,
        required : [ true, '3D Product Configurator is required'],
    },
    impressionyear: {
        type: String,
        required : [ true, 'Configurator Impressions year is required'],
    },
    assetsmanagement: {
        type: String,
        required : [ true, 'Digital Asset Management is required'],
    },
    augmentedreality: {
        type: String,
        required : [ true, 'Augmented Reality  is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_pricing', Pricing);
