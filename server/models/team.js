/********
 * team.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const Client = new Schema({
    name: {
        type: String,
        required : [ true, 'Name is required'],
    },
    designation: {
        type: String,
        required : [ true, 'Designation is required'],
    },
    description: {
        type: String,
        required : [ true, 'description is required'],
    },
    linkedin: {
        type: String,
        required : [ true, 'linkedin is required'],
    },
    file: {
        type: String,
        required : [ true, 'Image is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_team', Client);
