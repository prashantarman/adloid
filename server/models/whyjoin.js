/********
 * whyjoin.js file (models)
 ********/

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const CaaseStudy = new Schema({
    question: {
        type: String,
        required : [ true, 'Question is required'],
    },
    answer: {
        type: String,
        required : [ true, 'Answer is required'],
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('adloid_whyjoin', CaaseStudy);
