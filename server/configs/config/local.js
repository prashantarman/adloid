/*********
* local.js
*********/

let localConfig = {
  hostname: 'localhost',
  port: 3002
};

module.exports = localConfig;
