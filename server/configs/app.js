/*********
* app.js file
*********/


const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
let path = require('path');
const https = require('https');

module.exports = function () {
    let app = express(),
        create,
        start;

    create = (config, db) => {
        let routes = require('../routes');
        // set all the app things
        app.set('env', config.env);
        app.set('port', config.port);
        app.set('hostname', config.hostname);
        app.set('secretKey', db.secret);
        
        // add middleware to parse the json
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({
            extended: false
        }));

        app.all('/uploads/*', cors({"methods":"GET"}), (req,res, next) => {
            next();
        });
        app.use(express.static(path.join(__dirname, '../')));
        app.use(cors({"methods":"GET, POST, PUT, DELETE"}));

        app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            next();
        });



        //connect the database

        mongoose.connect(db.database,
            {
                useNewUrlParser:true,
                useFindAndModify: false,
                useUnifiedTopology: true
            })
            .then(_ => console.log('Connected Successfully to MongoDB'))
            .catch(err => console.error(err));
        mongoose.set('useCreateIndex', true);

        // Set up routes
        routes.init(app);
    };

    
    start = () => {
        let hostname = app.get('hostname'),
            port = app.get('port');

        app.listen(port, function () {
            console.log('Express app listening on - http://' + hostname + ':' + port);
        });
    };
    return {
        create: create,
        start: start
    };
};
