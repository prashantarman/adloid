import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TestimonialRoutingModule } from './testimonial-routing.module';
import { TestimonialComponent } from './testimonial.component';
import { AddTestimonialComponent } from './add-testimonial/add-testimonial.component';
import { SharedModule } from '../shared/shared.module';
import {AngularEditorModule} from '@kolkov/angular-editor';

@NgModule({
  declarations: [TestimonialComponent, AddTestimonialComponent],
  imports: [
    CommonModule,
    TestimonialRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AngularEditorModule
  ]
})
export class TestimonialModule { }
