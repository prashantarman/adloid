import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestimonialComponent } from './testimonial.component';
import {AddTestimonialComponent} from './add-testimonial/add-testimonial.component';

const routes: Routes = [
  { path: '', component: TestimonialComponent },
  { path: 'add', component: AddTestimonialComponent },
  { path: 'add/:id', component: AddTestimonialComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestimonialRoutingModule { }
