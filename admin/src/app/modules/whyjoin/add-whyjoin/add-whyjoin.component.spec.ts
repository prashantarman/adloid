import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddWhyjoinComponent } from './add-whyjoin.component';

describe('AddWhyjoinComponent', () => {
  let component: AddWhyjoinComponent;
  let fixture: ComponentFixture<AddWhyjoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddWhyjoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWhyjoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
