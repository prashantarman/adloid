import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { WhyjoinRoutingModule } from './whyjoin-routing.module';
import { WhyjoinComponent } from './whyjoin.component';
import { AddWhyjoinComponent } from './add-whyjoin/add-whyjoin.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [WhyjoinComponent, AddWhyjoinComponent],
  imports: [
    CommonModule,
    WhyjoinRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class WhyjoinModule { }
