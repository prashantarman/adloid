import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WhyjoinComponent } from './whyjoin.component';
import { AddWhyjoinComponent } from './add-whyjoin/add-whyjoin.component';

const routes: Routes = [
  { path: '', component: WhyjoinComponent },
  { path: 'add', component: AddWhyjoinComponent },
  { path: 'add/:id', component: AddWhyjoinComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WhyjoinRoutingModule { }
