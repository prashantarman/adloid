import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OurproductRoutingModule } from './ourproduct-routing.module';
import { OurproductComponent } from './ourproduct.component';
import { AddProductComponent } from './add-product/add-product.component';
import { SharedModule } from '../shared/shared.module';
import {AngularEditorModule} from '@kolkov/angular-editor';

@NgModule({
  declarations: [OurproductComponent, AddProductComponent],
  imports: [
    CommonModule,
    OurproductRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AngularEditorModule
  ]
})
export class OurproductModule { }
