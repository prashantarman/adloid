import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OurproductComponent } from './ourproduct.component';
import { AddProductComponent } from './add-product/add-product.component';

const routes: Routes = [
  { path: '', component: OurproductComponent },
  { path: 'add', component: AddProductComponent },
  { path: 'add/:id', component: AddProductComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OurproductRoutingModule { }
