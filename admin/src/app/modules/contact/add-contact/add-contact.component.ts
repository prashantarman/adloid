import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToasterService } from '../../../_services/toaster.service';
import {ActivatedRoute, Router} from '@angular/router';
import { CrudService } from '../../../_services/crud.service';
@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {

  addActivityForm: FormGroup;
  submitted = false;
  returnUrl: string;
  id = this.route.snapshot.params.id;
  contactList: any;

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private toaster: ToasterService, private crudService: CrudService) { }

  ngOnInit() {
    if (this.id !== undefined) {
      this.createEditActivityForm();

      this.getActivity(this.id);
    } else {
      this.createAddActivityForm();
    }
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  createAddActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      icon: ['', [Validators.required]],
      details: ['', [Validators.required]],
    });
  }
  createEditActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      icon: ['', [Validators.required]],
      details: ['', [Validators.required]],
    });
  }

  get f() { return this.addActivityForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addActivityForm.invalid) {
      return;
    }

    const addPayload = {
      title: this.addActivityForm.controls.title.value,
      icon: this.addActivityForm.controls.icon.value,
      details: this.addActivityForm.controls.details.value,
    };
    // console.log(addPayload);
    if (this.id !== undefined) {
      this.editActivity(this.id, addPayload);
    } else {
      this.addActivity(addPayload);
    }
  }


  addActivity(payload) {
    this.crudService.add('contacts', payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Add Activity', data.message);
            this.router.navigate(['/contact']);
          } else {
            this.toaster.showError('Add Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Add Activity', error);
        });
  }

  editActivity(id, payload) {
    this.crudService.edit(`contacts/${id}`, payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Edit Activity', data.message);
            this.router.navigate(['/contact']);
          } else {
            this.toaster.showError('Edit Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Edit Activity', error);
        });
  }

  getActivity(id) {
    this.crudService.getAll(`contacts/${id}`)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactList = data.data;

            this.addActivityForm.controls.title.setValue(this.contactList.title);
            this.addActivityForm.controls.icon.setValue(this.contactList.icon);
            this.addActivityForm.controls.details.setValue(this.contactList.details);
          } else {
            this.toaster.showError('Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

}
