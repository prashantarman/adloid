import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { ToasterService } from '../../_services/toaster.service';
import { CrudService } from '../../_services/crud.service';
declare var $: any;
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactLists: any;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService, private toaster: ToasterService) { }

  ngOnInit() {
    this.contactList();
  }

  contactList() {
    this.crudService.getAll('contacts')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactLists = data.data;
            // this.toaster.showSuccess('Contact', data.message);
            setTimeout(() => {
              $(() => {
                $('#dataTable').DataTable();
              });
            }, 1000);
          } else {
            setTimeout(() => {
              $(() => {
                $('#dataTable').DataTable();
              });
            }, 1000);
            this.toaster.showError('Contact', data.message);
          }
        },
        error => {
          setTimeout(() => {
            $(() => {
              $('#dataTable').DataTable();
            });
          }, 1000);
          this.toaster.showError('Contact', error);
        });
  }

  onDelete(id) {
    if (!confirm('Are you sure to delete')) {
      return;
    }
    this.deleteActivity(id);
  }

  deleteActivity(id) {
    this.crudService.delete(`contacts/${id}`)
      .subscribe(
        (response: any) => {
          console.log(response);
          if (response && response.code === 'SUCCESS') {
            this.toaster.showSuccess('Activity', response.message);
            this.reloadComponent();
          } else {
            this.toaster.showError('Activity', response.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

  reloadComponent() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['/contact']);
  }

}
