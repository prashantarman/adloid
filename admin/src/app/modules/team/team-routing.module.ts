import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamComponent } from './team.component';
import { AddTeamComponent } from './add-team/add-team.component';

const routes: Routes = [
  { path: '', component: TeamComponent },
  { path: 'add', component: AddTeamComponent },
  { path: 'add/:id', component: AddTeamComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
