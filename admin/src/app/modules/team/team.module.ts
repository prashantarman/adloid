import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TeamRoutingModule } from './team-routing.module';
import { TeamComponent } from './team.component';
import { AddTeamComponent } from './add-team/add-team.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TeamComponent, AddTeamComponent],
  imports: [
    CommonModule,
    TeamRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TeamModule { }
