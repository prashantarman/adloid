import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SliderRoutingModule } from './slider-routing.module';
import { SliderComponent } from './slider.component';
import { AddSliderComponent } from './add-slider/add-slider.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SliderComponent, AddSliderComponent],
  imports: [
    CommonModule,
    SliderRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SliderModule { }
