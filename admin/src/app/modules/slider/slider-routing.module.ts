import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SliderComponent } from './slider.component';
import { AddSliderComponent } from './add-slider/add-slider.component';

const routes: Routes = [
  { path: '', component: SliderComponent },
  { path: 'add', component: AddSliderComponent },
  { path: 'add/:id', component: AddSliderComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SliderRoutingModule { }
