import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from '../../../_services/toaster.service';
import {CrudService} from '../../../_services/crud.service';

@Component({
  selector: 'app-add-slider',
  templateUrl: './add-slider.component.html',
  styleUrls: ['./add-slider.component.scss']
})
export class AddSliderComponent implements OnInit {

  addActivityForm: FormGroup;
  submitted = false;
  returnUrl: string;
  id = this.route.snapshot.params.id;
  contactList: any;
  selectedfile: File = null;

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private toaster: ToasterService, private crudService: CrudService) { }

  ngOnInit() {
    if (this.id !== undefined) {
      this.createEditActivityForm();

      this.getActivity(this.id);
    } else {
      this.createAddActivityForm();
    }
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  createAddActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      link: ['', [Validators.required]],
      file: ['', [Validators.required]],
    });
  }
  createEditActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      link: ['', [Validators.required]],
      file: [''],
    });
  }

  get f() { return this.addActivityForm.controls; }

  fileUpload(event) {
    this.selectedfile = event.target.files[0];

    this.addActivityForm.controls.file.clearValidators();
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addActivityForm.invalid) {
      return;
    }

    const addPayload = {
      link: this.addActivityForm.controls.link.value
    };
    const formdata = new FormData();

    formdata.append('link', addPayload.link);
    if (this.selectedfile !== null) {
      formdata.append('image', this.selectedfile);
    }
    // console.log(addPayload);
    if (this.id !== undefined) {
      this.editActivity(this.id, formdata);
    } else {
      this.addActivity(formdata);
    }
  }


  addActivity(payload) {
    this.crudService.add('slider', payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Add Activity', data.message);
            this.router.navigate(['/slider']);
          } else {
            this.toaster.showError('Add Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Add Activity', error);
        });
  }

  editActivity(id, payload) {
    this.crudService.edit(`client/${id}`, payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Edit Activity', data.message);
            this.router.navigate(['/slider']);
          } else {
            this.toaster.showError('Edit Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Edit Activity', error);
        });
  }

  getActivity(id) {
    this.crudService.getAll(`slider/${id}`)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactList = data.data;
            this.addActivityForm.controls.link.setValue(this.contactList.link);
            // this.addActivityForm.controls.details.setValue(this.contactList.details);
          } else {
            this.toaster.showError('Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

}
