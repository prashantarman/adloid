import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {CrudService} from '../../_services/crud.service';
import {ToasterService} from '../../_services/toaster.service';
declare var $: any;

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  caseStudyLists: any;
  baseUrl: string = environment.baseUrl;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService, private toaster: ToasterService) { }

  ngOnInit() {
    this.caseStudyList();
  }

  caseStudyList() {
    this.crudService.getAll('slider')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.caseStudyLists = data.data;
            // this.toaster.showSuccess('Contact', data.message);
            setTimeout(() => {
              $(() => {
                $('#dataTable').DataTable();
              });
            }, 1000);
          } else {
            setTimeout(() => {
              $(() => {
                $('#dataTable').DataTable();
              });
            }, 1000);
            this.toaster.showError('slider', data.message);
          }
        },
        error => {
          setTimeout(() => {
            $(() => {
              $('#dataTable').DataTable();
            });
          }, 1000);
          this.toaster.showError('slider', error);
        });
  }

  onDelete(id) {
    if (!confirm('Are you sure to delete')) {
      return;
    }
    this.deleteActivity(id);
  }

  deleteActivity(id) {
    this.crudService.delete(`slider/${id}`)
      .subscribe(
        (response: any) => {
          console.log(response);
          if (response && response.code === 'SUCCESS') {
            this.toaster.showSuccess('Activity', response.message);
            this.reloadComponent();
          } else {
            this.toaster.showError('Activity', response.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

  reloadComponent() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['/slider']);
  }

}
