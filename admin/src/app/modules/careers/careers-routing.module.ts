import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CareersComponent } from './careers.component';
import { AddCareerComponent } from './add-career/add-career.component';

const routes: Routes = [
  { path: '', component: CareersComponent },
  { path: 'add', component: AddCareerComponent },
  { path: 'add/:id', component: AddCareerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CareersRoutingModule { }
