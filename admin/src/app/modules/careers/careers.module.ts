import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CareersRoutingModule } from './careers-routing.module';
import { CareersComponent } from './careers.component';
import { AddCareerComponent } from './add-career/add-career.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [CareersComponent, AddCareerComponent],
  imports: [
    CommonModule,
    CareersRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CareersModule { }
