import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from '../../../_services/toaster.service';
import {CrudService} from '../../../_services/crud.service';

@Component({
  selector: 'app-add-career',
  templateUrl: './add-career.component.html',
  styleUrls: ['./add-career.component.scss']
})
export class AddCareerComponent implements OnInit {

  addActivityForm: FormGroup;
  submitted = false;
  returnUrl: string;
  id = this.route.snapshot.params.id;
  contactList: any;
  selectedfile: Array<File> = [];
  selectedfile1: Array<File> = [];

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private toaster: ToasterService, private crudService: CrudService) { }

  ngOnInit() {
    if (this.id !== undefined) {
      this.createEditActivityForm();

      this.getActivity(this.id);
    } else {
      this.createAddActivityForm();
    }
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  createAddActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      file: ['', [Validators.required]],
      file1: ['', [Validators.required]],
    });
  }
  createEditActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      file: [''],
      file1: [''],
    });
  }

  get f() { return this.addActivityForm.controls; }

  fileUpload(event) {
    this.selectedfile = event.target.files as Array<File>;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.selectedfile.length; i++) {
      this.addActivityForm.controls.file.clearValidators();
      return true;
    }
  }

  fileUpload1(event) {
    this.selectedfile1 = event.target.files as Array<File>;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.selectedfile1.length; i++) {
      this.addActivityForm.controls.file1.clearValidators();
      return true;
    }
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addActivityForm.invalid) {
      return;
    }

    const addPayload = {
      title: this.addActivityForm.controls.title.value,
      description: this.addActivityForm.controls.description.value,
    };
    const formdata = new FormData();

    formdata.append('title', addPayload.title);
    formdata.append('description', addPayload.description);
    if (this.selectedfile !== null) {
      // formdata.append('image', this.selectedfile);
      if (this.selectedfile) {
        const files: Array<File> =  this.selectedfile;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < files.length; i++) {
          formdata.append('image', files[i], files[i].name);
        }
      }
    }
    if (this.selectedfile1 !== null) {
      if (this.selectedfile1) {
        const files: Array<File> =  this.selectedfile1;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < files.length; i++) {
          formdata.append('image1', files[i], files[i].name);
        }
      }
    }
    // console.log(addPayload);
    if (this.id !== undefined) {
      this.editActivity(this.id, formdata);
    } else {
      this.addActivity(formdata);
    }
  }


  addActivity(payload) {
    this.crudService.add('careers', payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Add Activity', data.message);
            this.router.navigate(['/careers']);
          } else {
            this.toaster.showError('Add Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Add Activity', error);
        });
  }

  editActivity(id, payload) {
    this.crudService.edit(`careers/${id}`, payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Edit Activity', data.message);
            this.router.navigate(['/careers']);
          } else {
            this.toaster.showError('Edit Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Edit Activity', error);
        });
  }

  getActivity(id) {
    this.crudService.getAll(`careers/${id}`)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactList = data.data;
            this.addActivityForm.controls.title.setValue(this.contactList.title);
            this.addActivityForm.controls.description.setValue(this.contactList.description);
          } else {
            this.toaster.showError('Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

}
