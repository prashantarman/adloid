import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddArsolutionComponent } from './add-arsolution.component';

describe('AddArsolutionComponent', () => {
  let component: AddArsolutionComponent;
  let fixture: ComponentFixture<AddArsolutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddArsolutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddArsolutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
