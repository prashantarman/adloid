import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from '../../../_services/toaster.service';
import {CrudService} from '../../../_services/crud.service';

@Component({
  selector: 'app-add-arsolution',
  templateUrl: './add-arsolution.component.html',
  styleUrls: ['./add-arsolution.component.scss']
})
export class AddArsolutionComponent implements OnInit {

  addActivityForm: FormGroup;
  submitted = false;
  returnUrl: string;
  id = this.route.snapshot.params.id;
  contactList: any;

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private toaster: ToasterService, private crudService: CrudService) { }

  ngOnInit() {
    if (this.id !== undefined) {
      this.createEditActivityForm();

      this.getActivity(this.id);
    } else {
      this.createAddActivityForm();
    }
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  createAddActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      icon: ['', [Validators.required]],
      iconTitle: ['', [Validators.required]],
      iconDescription: ['', [Validators.required]],
      icon1: ['', [Validators.required]],
      iconTitle1: ['', [Validators.required]],
      iconDescription1: ['', [Validators.required]],
      icon2: ['', [Validators.required]],
      iconTitle2: ['', [Validators.required]],
      iconDescription2: ['', [Validators.required]],
      lastTitle: ['', [Validators.required]],
      lastDescription: ['', [Validators.required]],
      lastTitle1: ['', [Validators.required]],
      lastDescription1: ['', [Validators.required]],
      lastTitle2: ['', [Validators.required]],
      lastDescription2: ['', [Validators.required]],
      status: ['', [Validators.required]]
    });
  }
  createEditActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      icon: ['', [Validators.required]],
      iconTitle: ['', [Validators.required]],
      iconDescription: ['', [Validators.required]],
      icon1: ['', [Validators.required]],
      iconTitle1: ['', [Validators.required]],
      iconDescription1: ['', [Validators.required]],
      icon2: ['', [Validators.required]],
      iconTitle2: ['', [Validators.required]],
      iconDescription2: ['', [Validators.required]],
      lastTitle: ['', [Validators.required]],
      lastDescription: ['', [Validators.required]],
      lastTitle1: ['', [Validators.required]],
      lastDescription1: ['', [Validators.required]],
      lastTitle2: ['', [Validators.required]],
      lastDescription2: ['', [Validators.required]],
      status: ['', [Validators.required]]
    });
  }

  get f() { return this.addActivityForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addActivityForm.invalid) {
      return;
    }

    const addPayload = {
      icon: this.addActivityForm.controls.icon.value,
      iconTitle: this.addActivityForm.controls.iconTitle.value,
      iconDescription: this.addActivityForm.controls.iconDescription.value,
      icon1: this.addActivityForm.controls.icon1.value,
      iconTitle1: this.addActivityForm.controls.iconTitle1.value,
      iconDescription1: this.addActivityForm.controls.iconDescription1.value,
      icon2: this.addActivityForm.controls.icon2.value,
      iconTitle2: this.addActivityForm.controls.iconTitle2.value,
      iconDescription2: this.addActivityForm.controls.iconDescription2.value,
      lastTitle: this.addActivityForm.controls.lastTitle.value,
      lastDescription: this.addActivityForm.controls.lastDescription.value,
      lastTitle1: this.addActivityForm.controls.lastTitle1.value,
      lastDescription1: this.addActivityForm.controls.lastDescription1.value,
      lastTitle2: this.addActivityForm.controls.lastTitle2.value,
      lastDescription2: this.addActivityForm.controls.lastDescription2.value,
      status: this.addActivityForm.controls.status.value,
    };
    // console.log(addPayload);
    if (this.id !== undefined) {
      this.editActivity(this.id, addPayload);
    } else {
      this.addActivity(addPayload);
    }
  }


  addActivity(payload) {
    this.crudService.add('arsolutions', payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Add Activity', data.message);
            this.router.navigate(['/arsolution']);
          } else {
            this.toaster.showError('Add Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Add Activity', error);
        });
  }

  editActivity(id, payload) {
    this.crudService.edit(`arsolutions/${id}`, payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Edit Activity', data.message);
            this.router.navigate(['/arsolution']);
          } else {
            this.toaster.showError('Edit Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Edit Activity', error);
        });
  }

  getActivity(id) {
    this.crudService.getAll(`arsolutions/${id}`)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactList = data.data;
            this.addActivityForm.controls.icon.setValue(this.contactList.icon);
            this.addActivityForm.controls.iconTitle.setValue(this.contactList.iconTitle);
            this.addActivityForm.controls.iconDescription.setValue(this.contactList.iconDescription);
            this.addActivityForm.controls.icon1.setValue(this.contactList.icon1);
            this.addActivityForm.controls.iconTitle1.setValue(this.contactList.iconTitle1);
            this.addActivityForm.controls.iconDescription1.setValue(this.contactList.iconDescription1);
            this.addActivityForm.controls.icon2.setValue(this.contactList.icon2);
            this.addActivityForm.controls.iconTitle2.setValue(this.contactList.iconTitle2);
            this.addActivityForm.controls.iconDescription2.setValue(this.contactList.iconDescription2);
            this.addActivityForm.controls.lastTitle.setValue(this.contactList.lastTitle);
            this.addActivityForm.controls.lastDescription.setValue(this.contactList.lastDescription);
            this.addActivityForm.controls.lastTitle1.setValue(this.contactList.lastTitle1);
            this.addActivityForm.controls.lastDescription1.setValue(this.contactList.lastDescription1);
            this.addActivityForm.controls.lastTitle2.setValue(this.contactList.lastTitle2);
            this.addActivityForm.controls.lastDescription2.setValue(this.contactList.lastDescription2);
            this.addActivityForm.controls.status.setValue(this.contactList.status);
          } else {
            this.toaster.showError('Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

}
