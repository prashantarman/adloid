import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArsolutionComponent } from './arsolution.component';
import { AddArsolutionComponent } from './add-arsolution/add-arsolution.component';

const routes: Routes = [
  { path: '', component: ArsolutionComponent },
  { path: 'add', component: AddArsolutionComponent },
  { path: 'add/:id', component: AddArsolutionComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArsolutionRoutingModule { }
