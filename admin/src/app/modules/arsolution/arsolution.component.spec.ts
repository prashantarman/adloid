import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArsolutionComponent } from './arsolution.component';

describe('ArsolutionComponent', () => {
  let component: ArsolutionComponent;
  let fixture: ComponentFixture<ArsolutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArsolutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArsolutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
