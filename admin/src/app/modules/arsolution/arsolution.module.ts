import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ArsolutionRoutingModule } from './arsolution-routing.module';
import { ArsolutionComponent } from './arsolution.component';
import { AddArsolutionComponent } from './add-arsolution/add-arsolution.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ArsolutionComponent, AddArsolutionComponent],
  imports: [
    CommonModule,
    ArsolutionRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ArsolutionModule { }
