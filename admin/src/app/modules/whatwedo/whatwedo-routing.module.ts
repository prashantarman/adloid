import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WhatwedoComponent } from './whatwedo.component';
import { AddWhatwedoComponent } from './add-whatwedo/add-whatwedo.component';

const routes: Routes = [
  { path: '', component: WhatwedoComponent },
  { path: 'add', component: AddWhatwedoComponent },
  { path: 'add/:id', component: AddWhatwedoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WhatwedoRoutingModule { }
