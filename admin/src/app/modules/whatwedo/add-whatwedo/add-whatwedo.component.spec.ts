import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddWhatwedoComponent } from './add-whatwedo.component';

describe('AddWhatwedoComponent', () => {
  let component: AddWhatwedoComponent;
  let fixture: ComponentFixture<AddWhatwedoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddWhatwedoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWhatwedoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
