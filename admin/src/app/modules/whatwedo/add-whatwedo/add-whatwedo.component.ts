import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from '../../../_services/toaster.service';
import {CrudService} from '../../../_services/crud.service';

@Component({
  selector: 'app-add-whatwedo',
  templateUrl: './add-whatwedo.component.html',
  styleUrls: ['./add-whatwedo.component.scss']
})
export class AddWhatwedoComponent implements OnInit {

  addActivityForm: FormGroup;
  submitted = false;
  returnUrl: string;
  id = this.route.snapshot.params.id;
  contactList: any;
  selectedfile: File = null;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '300px',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };


  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private toaster: ToasterService, private crudService: CrudService) { }

  ngOnInit() {

    if (this.id !== undefined) {
      this.createEditActivityForm();

      this.getActivity(this.id);
    } else {
      this.createAddActivityForm();
    }
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }


  createAddActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      subtitle: ['', [Validators.required]],
      status: ['', [Validators.required]],
      file: ['', [Validators.required]],
    });
  }
  createEditActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      subtitle: ['', [Validators.required]],
      status: ['', [Validators.required]],
      file: [''],
    });
  }

  get f() { return this.addActivityForm.controls; }

  fileUpload(event) {
    this.selectedfile = event.target.files[0];

    this.addActivityForm.controls.file.clearValidators();
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addActivityForm.invalid) {
      return;
    }

    const addPayload = {
      title: this.addActivityForm.controls.title.value,
      description: this.addActivityForm.controls.description.value,
      subtitle: this.addActivityForm.controls.subtitle.value,
      status: this.addActivityForm.controls.status.value,
    };
    const formdata = new FormData();

    formdata.append('title', addPayload.title);
    formdata.append('description', addPayload.description);
    formdata.append('subtitle', addPayload.subtitle);
    formdata.append('status', addPayload.status);
    if (this.selectedfile !== null) {
      formdata.append('image', this.selectedfile);
    }
    // console.log(addPayload);
    if (this.id !== undefined) {
      this.editActivity(this.id, formdata);
    } else {
      this.addActivity(formdata);
    }
  }


  addActivity(payload) {
    this.crudService.add('whatwedo', payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Add Activity', data.message);
            this.router.navigate(['/whatwedo']);
          } else {
            this.toaster.showError('Add Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Add Activity', error);
        });
  }

  editActivity(id, payload) {
    this.crudService.edit(`whatwedo/${id}`, payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Edit Activity', data.message);
            this.router.navigate(['/whatwedo']);
          } else {
            this.toaster.showError('Edit Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Edit Activity', error);
        });
  }

  getActivity(id) {
    this.crudService.getAll(`whatwedo/${id}`)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactList = data.data;
            this.addActivityForm.controls.title.setValue(this.contactList.title);
            this.addActivityForm.controls.description.setValue(this.contactList.description);
            this.addActivityForm.controls.subtitle.setValue(this.contactList.subtitle);
            this.addActivityForm.controls.status.setValue(this.contactList.status);
          } else {
            this.toaster.showError('Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

}
