import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { WhatwedoRoutingModule } from './whatwedo-routing.module';
import { WhatwedoComponent } from './whatwedo.component';
import { AddWhatwedoComponent } from './add-whatwedo/add-whatwedo.component';
import { SharedModule } from '../shared/shared.module';
import {AngularEditorModule} from '@kolkov/angular-editor';

@NgModule({
  declarations: [WhatwedoComponent, AddWhatwedoComponent],
  imports: [
    CommonModule,
    WhatwedoRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AngularEditorModule
  ]
})
export class WhatwedoModule { }
