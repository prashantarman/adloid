import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from '../../../_services/toaster.service';
import {CrudService} from '../../../_services/crud.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  addActivityForm: FormGroup;
  submitted = false;
  returnUrl: string;
  id = this.route.snapshot.params.id;
  contactList: any;
  selectedfile: File = null;

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private toaster: ToasterService, private crudService: CrudService) { }

  ngOnInit() {
    if (this.id !== undefined) {
      this.createEditActivityForm();

      this.getActivity(this.id);
    } else {
      this.createAddActivityForm();
    }
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  createAddActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      file: ['', [Validators.required]],
      icon: ['', [Validators.required]],
      iconTitle: ['', [Validators.required]],
      iconDescription: ['', [Validators.required]],
      icon1: ['', [Validators.required]],
      iconTitle1: ['', [Validators.required]],
      iconDescription1: ['', [Validators.required]],
      icon2: ['', [Validators.required]],
      iconTitle2: ['', [Validators.required]],
      iconDescription2: ['', [Validators.required]],
      lastTitle: ['', [Validators.required]],
      lastDescription: ['', [Validators.required]],
      lastTitle1: ['', [Validators.required]],
      lastDescription1: ['', [Validators.required]],
      lastTitle2: ['', [Validators.required]],
      lastDescription2: ['', [Validators.required]],
      bgColor: ['', [Validators.required]]
    });
  }
  createEditActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      file: [''],
      icon: ['', [Validators.required]],
      iconTitle: ['', [Validators.required]],
      iconDescription: ['', [Validators.required]],
      icon1: ['', [Validators.required]],
      iconTitle1: ['', [Validators.required]],
      iconDescription1: ['', [Validators.required]],
      icon2: ['', [Validators.required]],
      iconTitle2: ['', [Validators.required]],
      iconDescription2: ['', [Validators.required]],
      lastTitle: ['', [Validators.required]],
      lastDescription: ['', [Validators.required]],
      lastTitle1: ['', [Validators.required]],
      lastDescription1: ['', [Validators.required]],
      lastTitle2: ['', [Validators.required]],
      lastDescription2: ['', [Validators.required]],
      bgColor: ['', [Validators.required]]
    });
  }

  get f() { return this.addActivityForm.controls; }

  fileUpload(event) {
    this.selectedfile = event.target.files[0];
    this.addActivityForm.controls.file.clearValidators();
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addActivityForm.invalid) {
      return;
    }

    const addPayload = {
      title: this.addActivityForm.controls.title.value,
      description: this.addActivityForm.controls.description.value,
      icon: this.addActivityForm.controls.icon.value,
      iconTitle: this.addActivityForm.controls.iconTitle.value,
      iconDescription: this.addActivityForm.controls.iconDescription.value,
      icon1: this.addActivityForm.controls.icon1.value,
      iconTitle1: this.addActivityForm.controls.iconTitle1.value,
      iconDescription1: this.addActivityForm.controls.iconDescription1.value,
      icon2: this.addActivityForm.controls.icon2.value,
      iconTitle2: this.addActivityForm.controls.iconTitle2.value,
      iconDescription2: this.addActivityForm.controls.iconDescription2.value,
      lastTitle: this.addActivityForm.controls.lastTitle.value,
      lastDescription: this.addActivityForm.controls.lastDescription.value,
      lastTitle1: this.addActivityForm.controls.lastTitle1.value,
      lastDescription1: this.addActivityForm.controls.lastDescription1.value,
      lastTitle2: this.addActivityForm.controls.lastTitle2.value,
      lastDescription2: this.addActivityForm.controls.lastDescription2.value,
      bgColor: this.addActivityForm.controls.bgColor.value,
    };

    const formdata = new FormData();

    formdata.append('title', addPayload.title);
    formdata.append('description', addPayload.description);
    formdata.append('icon', addPayload.icon);
    formdata.append('iconTitle', addPayload.iconTitle);
    formdata.append('iconDescription', addPayload.iconDescription);
    formdata.append('icon1', addPayload.icon1);
    formdata.append('iconTitle1', addPayload.iconTitle1);
    formdata.append('iconDescription1', addPayload.iconDescription1);
    formdata.append('icon2', addPayload.icon2);
    formdata.append('iconTitle2', addPayload.iconTitle2);
    formdata.append('iconDescription2', addPayload.iconDescription2);
    formdata.append('lastTitle', addPayload.lastTitle);
    formdata.append('lastDescription', addPayload.lastDescription);
    formdata.append('lastTitle1', addPayload.lastTitle1);
    formdata.append('lastDescription1', addPayload.lastDescription1);
    formdata.append('lastTitle2', addPayload.lastTitle2);
    formdata.append('lastDescription2', addPayload.lastDescription2);
    formdata.append('bgColor', addPayload.bgColor);
    if (this.selectedfile !== null) {
      formdata.append('image', this.selectedfile);
    }

    // console.log(addPayload);
    if (this.id !== undefined) {
      this.editActivity(this.id, formdata);
    } else {
      this.addActivity(formdata);
    }
  }


  addActivity(payload) {
    this.crudService.add('product', payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Add Activity', data.message);
            this.router.navigate(['/products']);
          } else {
            this.toaster.showError('Add Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Add Activity', error);
        });
  }

  editActivity(id, payload) {
    this.crudService.edit(`product/${id}`, payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Edit Activity', data.message);
            this.router.navigate(['/products']);
          } else {
            this.toaster.showError('Edit Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Edit Activity', error);
        });
  }

  getActivity(id) {
    this.crudService.getAll(`product/${id}`)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactList = data.data;
            this.addActivityForm.controls.title.setValue(this.contactList.title);
            this.addActivityForm.controls.description.setValue(this.contactList.description);
            this.addActivityForm.controls.icon.setValue(this.contactList.icon);
            this.addActivityForm.controls.iconTitle.setValue(this.contactList.iconTitle);
            this.addActivityForm.controls.iconDescription.setValue(this.contactList.iconDescription);
            this.addActivityForm.controls.icon1.setValue(this.contactList.icon1);
            this.addActivityForm.controls.iconTitle1.setValue(this.contactList.iconTitle1);
            this.addActivityForm.controls.iconDescription1.setValue(this.contactList.iconDescription1);
            this.addActivityForm.controls.icon2.setValue(this.contactList.icon2);
            this.addActivityForm.controls.iconTitle2.setValue(this.contactList.iconTitle2);
            this.addActivityForm.controls.iconDescription2.setValue(this.contactList.iconDescription2);
            this.addActivityForm.controls.lastTitle.setValue(this.contactList.lastTitle);
            this.addActivityForm.controls.lastDescription.setValue(this.contactList.lastDescription);
            this.addActivityForm.controls.lastTitle1.setValue(this.contactList.lastTitle1);
            this.addActivityForm.controls.lastDescription1.setValue(this.contactList.lastDescription1);
            this.addActivityForm.controls.lastTitle2.setValue(this.contactList.lastTitle2);
            this.addActivityForm.controls.lastDescription2.setValue(this.contactList.lastDescription2);
            this.addActivityForm.controls.bgColor.setValue(this.contactList.bgColor);
          } else {
            this.toaster.showError('Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

}
