import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { ToasterService } from '../../_services/toaster.service';
import { CrudService } from '../../_services/crud.service';
declare var $: any;

@Component({
  selector: 'app-openings',
  templateUrl: './openings.component.html',
  styleUrls: ['./openings.component.scss']
})
export class OpeningsComponent implements OnInit {

  caseStudyLists: any;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService, private toaster: ToasterService) { }

  ngOnInit() {
    this.caseStudyList();
  }

  caseStudyList() {
    this.crudService.getAll('currentOpenings')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.caseStudyLists = data.data;
            // this.toaster.showSuccess('Contact', data.message);
            setTimeout(() => {
              $(() => {
                $('#dataTable').DataTable();
              });
            }, 1000);
          } else {
            setTimeout(() => {
              $(() => {
                $('#dataTable').DataTable();
              });
            }, 1000);
            this.toaster.showError('currentOpenings', data.message);
          }
        },
        error => {
          setTimeout(() => {
            $(() => {
              $('#dataTable').DataTable();
            });
          }, 1000);
          this.toaster.showError('currentOpenings', error);
        });
  }

  onDelete(id) {
    if (!confirm('Are you sure to delete')) {
      return;
    }
    this.deleteActivity(id);
  }

  deleteActivity(id) {
    this.crudService.delete(`currentOpenings/${id}`)
      .subscribe(
        (response: any) => {
          console.log(response);
          if (response && response.code === 'SUCCESS') {
            this.toaster.showSuccess('Activity', response.message);
            this.reloadComponent();
          } else {
            this.toaster.showError('Activity', response.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

  reloadComponent() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['/openings']);
  }

}
