import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpeningsComponent } from './openings.component';
import { AddOpeningComponent } from './add-opening/add-opening.component';

const routes: Routes = [
  { path: '', component: OpeningsComponent },
  { path: 'add', component: AddOpeningComponent },
  { path: 'add/:id', component: AddOpeningComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpeningsRoutingModule { }
