import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OpeningsRoutingModule } from './openings-routing.module';
import { OpeningsComponent } from './openings.component';
import { AddOpeningComponent } from './add-opening/add-opening.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [OpeningsComponent, AddOpeningComponent],
  imports: [
    CommonModule,
    OpeningsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class OpeningsModule { }
