import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { AddBlogComponent } from './add-blog/add-blog.component';
import { SharedModule } from '../shared/shared.module';
import {AngularEditorModule} from '@kolkov/angular-editor';

@NgModule({
  declarations: [BlogComponent, AddBlogComponent],
  imports: [
    CommonModule,
    BlogRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AngularEditorModule
  ]
})
export class BlogModule { }
