import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PricingComponent } from './pricing.component';
import { AddPricingComponent } from './add-pricing/add-pricing.component';

const routes: Routes = [
  { path: '', component: PricingComponent },
  { path: 'add', component: AddPricingComponent },
  { path: 'add/:id', component: AddPricingComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PricingRoutingModule { }
