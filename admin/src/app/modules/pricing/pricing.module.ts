import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PricingRoutingModule } from './pricing-routing.module';
import { PricingComponent } from './pricing.component';
import { AddPricingComponent } from './add-pricing/add-pricing.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PricingComponent, AddPricingComponent],
  imports: [
    CommonModule,
    PricingRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PricingModule { }
