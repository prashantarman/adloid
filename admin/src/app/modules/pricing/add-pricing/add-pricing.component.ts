import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from '../../../_services/toaster.service';
import {CrudService} from '../../../_services/crud.service';

@Component({
  selector: 'app-add-pricing',
  templateUrl: './add-pricing.component.html',
  styleUrls: ['./add-pricing.component.scss']
})
export class AddPricingComponent implements OnInit {

  addActivityForm: FormGroup;
  submitted = false;
  returnUrl: string;
  id = this.route.snapshot.params.id;
  contactList: any;

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private toaster: ToasterService, private crudService: CrudService) { }

  ngOnInit() {
    if (this.id !== undefined) {
      this.createEditActivityForm();

      this.getActivity(this.id);
    } else {
      this.createAddActivityForm();
    }
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  createAddActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      subtitle: ['', [Validators.required]],
      price: ['', [Validators.required]],
      prodconf: ['', [Validators.required]],
      impressionyear: ['', [Validators.required]],
      assetsmanagement: ['', [Validators.required]],
      augmentedreality: ['', [Validators.required]]
    });
  }
  createEditActivityForm() {
    this.addActivityForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      subtitle: ['', [Validators.required]],
      price: ['', [Validators.required]],
      prodconf: ['', [Validators.required]],
      impressionyear: ['', [Validators.required]],
      assetsmanagement: ['', [Validators.required]],
      augmentedreality: ['', [Validators.required]]
    });
  }

  get f() { return this.addActivityForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addActivityForm.invalid) {
      return;
    }

    const addPayload = {
      title: this.addActivityForm.controls.title.value,
      subtitle: this.addActivityForm.controls.subtitle.value,
      price: this.addActivityForm.controls.price.value,
      prodconf: this.addActivityForm.controls.prodconf.value,
      impressionyear: this.addActivityForm.controls.impressionyear.value,
      assetsmanagement: this.addActivityForm.controls.assetsmanagement.value,
      augmentedreality: this.addActivityForm.controls.augmentedreality.value,
    };
    // console.log(addPayload);
    if (this.id !== undefined) {
      this.editActivity(this.id, addPayload);
    } else {
      this.addActivity(addPayload);
    }
  }


  addActivity(payload) {
    this.crudService.add('pricing', payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Add Activity', data.message);
            this.router.navigate(['/pricing']);
          } else {
            this.toaster.showError('Add Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Add Activity', error);
        });
  }

  editActivity(id, payload) {
    this.crudService.edit(`pricing/${id}`, payload)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.toaster.showSuccess('Edit Activity', data.message);
            this.router.navigate(['/pricing']);
          } else {
            this.toaster.showError('Edit Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Edit Activity', error);
        });
  }

  getActivity(id) {
    this.crudService.getAll(`pricing/${id}`)
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactList = data.data;
            this.addActivityForm.controls.title.setValue(this.contactList.title);
            this.addActivityForm.controls.subtitle.setValue(this.contactList.subtitle);
            this.addActivityForm.controls.price.setValue(this.contactList.price);
            this.addActivityForm.controls.prodconf.setValue(this.contactList.prodconf);
            this.addActivityForm.controls.impressionyear.setValue(this.contactList.impressionyear);
            this.addActivityForm.controls.assetsmanagement.setValue(this.contactList.assetsmanagement);
            this.addActivityForm.controls.augmentedreality.setValue(this.contactList.augmentedreality);
          } else {
            this.toaster.showError('Activity', data.message);
          }
        },
        error => {
          this.toaster.showError('Activity', error);
        });
  }

}
