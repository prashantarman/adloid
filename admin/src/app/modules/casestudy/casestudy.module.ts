import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CasestudyRoutingModule } from './casestudy-routing.module';
import { CasestudyComponent } from './casestudy.component';
import { AddCasestudyComponent } from './add-casestudy/add-casestudy.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [CasestudyComponent, AddCasestudyComponent],
  imports: [
    CommonModule,
    CasestudyRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CasestudyModule { }
