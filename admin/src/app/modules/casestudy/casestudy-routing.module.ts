import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CasestudyComponent } from './casestudy.component';
import { AddCasestudyComponent } from './add-casestudy/add-casestudy.component';

const routes: Routes = [
  { path: '', component: CasestudyComponent },
  { path: 'add', component: AddCasestudyComponent },
  { path: 'add/:id', component: AddCasestudyComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CasestudyRoutingModule { }
