import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCasestudyComponent } from './add-casestudy.component';

describe('AddCasestudyComponent', () => {
  let component: AddCasestudyComponent;
  let fixture: ComponentFixture<AddCasestudyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCasestudyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCasestudyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
