import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientsComponent } from './clients.component';
import { AddClientComponent } from './add-client/add-client.component';

const routes: Routes = [
  { path: '', component: ClientsComponent },
  { path: 'add', component: AddClientComponent },
  { path: 'add/:id', component: AddClientComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
