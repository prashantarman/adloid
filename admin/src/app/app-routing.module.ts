import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AuthGuard } from './_helpers/auth.guard';

const routes: Routes = [
  {
    path: '', loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'dashboard', loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'contact', loadChildren: () => import('./modules/contact/contact.module').then(m => m.ContactModule)
  },
  { path: 'casestudy', loadChildren: () => import('./modules/casestudy/casestudy.module').then(m => m.CasestudyModule) },
  { path: 'clients', loadChildren: () => import('./modules/clients/clients.module').then(m => m.ClientsModule) },
  { path: 'pricing', loadChildren: () => import('./modules/pricing/pricing.module').then(m => m.PricingModule) },
  { path: 'products', loadChildren: () => import('./modules/products/products.module').then(m => m.ProductsModule) },
  { path: 'team', loadChildren: () => import('./modules/team/team.module').then(m => m.TeamModule) },
  { path: 'whyjoin', loadChildren: () => import('./modules/whyjoin/whyjoin.module').then(m => m.WhyjoinModule) },
  { path: 'careers', loadChildren: () => import('./modules/careers/careers.module').then(m => m.CareersModule) },
  { path: 'openings', loadChildren: () => import('./modules/openings/openings.module').then(m => m.OpeningsModule) },
  { path: 'arsolution', loadChildren: () => import('./modules/arsolution/arsolution.module').then(m => m.ArsolutionModule) },
  { path: 'blog', loadChildren: () => import('./modules/blog/blog.module').then(m => m.BlogModule) },
  { path: 'testimonial', loadChildren: () => import('./modules/testimonial/testimonial.module').then(m => m.TestimonialModule) },
  { path: 'ourproduct', loadChildren: () => import('./modules/ourproduct/ourproduct.module').then(m => m.OurproductModule) },
  { path: 'whatwedo', loadChildren: () => import('./modules/whatwedo/whatwedo.module').then(m => m.WhatwedoModule) },
  { path: 'slider', loadChildren: () => import('./modules/slider/slider.module').then(m => m.SliderModule) },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  { useHash: false, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
