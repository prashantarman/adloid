import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CrudService} from '../../_services/crud.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: any;
  baseUrl: string = environment.baseUrl;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService) { }

  ngOnInit() {
    this.productList();
  }

  get_url_extension( url ) {
    return url.split(/[#?]/)[0].split('.').pop().trim();
  }

  productList() {
    this.crudService.getAll('product')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.products = data.data;
          } else {
            this.products = [];
          }
        },
        error => {
          this.products = [];
        });
  }

}
