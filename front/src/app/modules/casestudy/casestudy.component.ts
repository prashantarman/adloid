import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { CrudService } from '../../_services/crud.service';

@Component({
  selector: 'app-casestudy',
  templateUrl: './casestudy.component.html',
  styleUrls: ['./casestudy.component.scss']
})
export class CasestudyComponent implements OnInit {

  caseStudyLists: any;
  delay = '0.2s';
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService) { }

  ngOnInit() {
    this.caseStudyList();
  }

  caseStudyList() {
    this.crudService.getAll('casestudy')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.caseStudyLists = data.data;
          } else {
            this.caseStudyLists = [];
          }
        },
        error => {
          this.caseStudyLists = [];
        });
  }

}
