import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CasestudyComponent } from './casestudy.component';

const routes: Routes = [{ path: '', component: CasestudyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CasestudyRoutingModule { }
