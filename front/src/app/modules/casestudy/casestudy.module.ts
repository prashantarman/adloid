import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasestudyRoutingModule } from './casestudy-routing.module';
import { CasestudyComponent } from './casestudy.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [CasestudyComponent],
  imports: [
    CommonModule,
    CasestudyRoutingModule,
    SharedModule
  ]
})
export class CasestudyModule { }
