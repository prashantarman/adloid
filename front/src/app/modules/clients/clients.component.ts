import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { CrudService } from '../../_services/crud.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  clientsLists: any;
  testimonials: any;
  baseUrl: string = environment.baseUrl;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService) { }

  ngOnInit() {
    this.clientsList();
    this.testimonialsList();
  }

  clientsList() {
    this.crudService.getAll('client')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.clientsLists = data.data;
          } else {
            this.clientsLists = [];
          }
        },
        error => {
          this.clientsLists = [];
        });
  }

  testimonialsList() {
    this.crudService.getAll('testimonial')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.testimonials = data.data;
          } else {
            this.testimonials = [];
          }
        },
        error => {
          this.testimonials = [];
        });
  }

}
