import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CrudService} from '../../_services/crud.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.scss']
})
export class CareersComponent implements OnInit {

  careers: any;
  teams: any;
  openings: any;
  whyJoin: any;
  baseUrl: string = environment.baseUrl;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService) { }

  ngOnInit() {
    this.careerList();
    this.teamList();
    this.openingsList();
    this.whyJoinList();
  }

  get_url_extension( url ) {
    return url.split(/[#?]/)[0].split('.').pop().trim();
  }

  careerList() {
    this.crudService.getAll('careers')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.careers = data.data;
          } else {
            this.careers = [];
          }
        },
        error => {
          this.careers = [];
        });
  }

  teamList() {
    this.crudService.getAll('team')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.teams = data.data;
          } else {
            this.teams = [];
          }
        },
        error => {
          this.teams = [];
        });
  }

  openingsList() {
    this.crudService.getAll('currentOpenings')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.openings = data.data;
          } else {
            this.openings = [];
          }
        },
        error => {
          this.openings = [];
        });
  }
  whyJoinList() {
    this.crudService.getAll('whyjoin')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.whyJoin = data.data;
          } else {
            this.whyJoin = [];
          }
        },
        error => {
          this.whyJoin = [];
        });
  }

}
