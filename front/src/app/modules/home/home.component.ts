import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { CrudService } from '../../_services/crud.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  products: any;
  sliders: any;
  whatwedo: any;
  testimonials: any;
  arsolutions: any;
  blogs: any;
  baseUrl: string = environment.baseUrl;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService) { }

  ngOnInit() {
    this.productList();
    this.sliderList();
    this.whatwedoList();
    this.testimonialsList();
    this.blogList();
    this.arSolutionsList();
  }

  get_url_extension( url ) {
    return url.split(/[#?]/)[0].split('.').pop().trim();
  }

  productList() {
    this.crudService.getAll('ourProduct')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.products = data.data;
          } else {
            this.products = [];
          }
        },
        error => {
          this.products = [];
        });
  }

  sliderList() {
    this.crudService.getAll('slider')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.sliders = data.data;
          } else {
            this.sliders = [];
          }
        },
        error => {
          this.sliders = [];
        });
  }

  whatwedoList() {
    this.crudService.getAll('whatwedo')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.whatwedo = data.data;
          } else {
            this.whatwedo = [];
          }
        },
        error => {
          this.whatwedo = [];
        });
  }

  testimonialsList() {
    this.crudService.getAll('testimonial')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.testimonials = data.data;
          } else {
            this.testimonials = [];
          }
        },
        error => {
          this.testimonials = [];
        });
  }

  blogList() {
    this.crudService.getAll('blog')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.blogs = data.data;
          } else {
            this.blogs = [];
          }
        },
        error => {
          this.blogs = [];
        });
  }

  arSolutionsList() {
    this.crudService.getAll('arsolutions')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.arsolutions = data.data;
          } else {
            this.arsolutions = [];
          }
        },
        error => {
          this.arsolutions = [];
        });
  }

}
