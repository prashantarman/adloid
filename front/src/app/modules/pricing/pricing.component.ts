import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { CrudService } from '../../_services/crud.service';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  pricingLists: any;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService) { }

  ngOnInit() {
    this.pricingList();
  }

  pricingList() {
    this.crudService.getAll('pricing')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.pricingLists = data.data;
          } else {
            this.pricingLists = [];
          }
        },
        error => {
          this.pricingLists = [];
        });
  }

}
