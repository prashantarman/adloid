import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubmitdetailsComponent } from './submitdetails.component';

const routes: Routes = [{ path: '', component: SubmitdetailsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubmitdetailsRoutingModule { }
