import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubmitdetailsRoutingModule } from './submitdetails-routing.module';
import { SubmitdetailsComponent } from './submitdetails.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SubmitdetailsComponent],
  imports: [
    CommonModule,
    SubmitdetailsRoutingModule,
    SharedModule
  ]
})
export class SubmitdetailsModule { }
