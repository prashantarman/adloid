import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { CrudService } from '../../_services/crud.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent implements OnInit {

  contactLists: any;
  constructor(private router: Router, private route: ActivatedRoute, private crudService: CrudService) { }

  ngOnInit() {
    this.contactList();
  }

  contactList() {
    this.crudService.getAll('contacts')
      .subscribe(
        (data: any) => {
          if (data && data.code === 'SUCCESS') {
            this.contactLists = data.data;
          } else {
            this.contactLists = [];
          }
        },
        error => {
          this.contactLists = [];
        });
  }

}
