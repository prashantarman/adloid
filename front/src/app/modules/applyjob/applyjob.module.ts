import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplyjobRoutingModule } from './applyjob-routing.module';
import { ApplyjobComponent } from './applyjob.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ApplyjobComponent],
  imports: [
    CommonModule,
    ApplyjobRoutingModule,
    SharedModule
  ]
})
export class ApplyjobModule { }
