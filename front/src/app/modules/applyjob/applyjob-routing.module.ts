import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplyjobComponent } from './applyjob.component';

const routes: Routes = [{ path: '', component: ApplyjobComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplyjobRoutingModule { }
