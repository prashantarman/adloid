import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) { }

  private baseUrl: string = environment.apiUrl;

  private static handleError(error: HttpErrorResponse) {
    let errorMessage: string;
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }


  add(Endpoint, Payload) {
    return this.http.post<any>(`${this.baseUrl}/${Endpoint}`, Payload)
      .pipe(map(res => {
          return res;
        }),
        catchError(CrudService.handleError));

  }
  addWithProgress(Endpoint, Payload): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/${Endpoint}`, Payload, {reportProgress: true, observe: 'events'})
      .pipe(map(res => {
          return res;
        }),
        catchError(CrudService.handleError));

  }

  edit(Endpoint, Payload) {
    return this.http.put<any>(`${this.baseUrl}/${Endpoint}`, Payload)
      .pipe(map(res => {
          return res;
        }),
        catchError(CrudService.handleError));

  }
  editWithProgress(Endpoint, Payload): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/${Endpoint}`, Payload, {reportProgress: true, observe: 'events'})
      .pipe(map(res => {
          return res;
        }),
        catchError(CrudService.handleError));

  }

  getById(Endpoint, Payload) {
    return this.http.get<any>(`${this.baseUrl}/${Endpoint}/${Payload}`)
      .pipe(map(res => {
          return res;
        }),
        catchError(CrudService.handleError));

  }

  delete(Endpoint) {
    return this.http.delete<any>(`${this.baseUrl}/${Endpoint}`)
      .pipe(map(res => {
          return res;
        }),
        catchError(CrudService.handleError));


  }

  getAll(Endpoint): Observable<any> {
    return this.http.get(`${this.baseUrl}/${Endpoint}`).pipe(
      map((res) => {
        return res;
      }),
      catchError(CrudService.handleError));
  }

  getConditional(Endpoint, Payload): Observable<any> {
    return this.http.post(`${this.baseUrl}/${Endpoint}`, Payload).pipe(
      map((res) => {
        return res;
      }),
      catchError(CrudService.handleError));
  }
}
