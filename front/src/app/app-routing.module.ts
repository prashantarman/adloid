import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'products', loadChildren: () => import('./modules/products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'clients', loadChildren: () => import('./modules/clients/clients.module').then(m => m.ClientsModule)
  },
  {
    path: 'pricing', loadChildren: () => import('./modules/pricing/pricing.module').then(m => m.PricingModule)
  },
  {
    path: 'careers', loadChildren: () => import('./modules/careers/careers.module').then(m => m.CareersModule)
  },
  { path: 'casestudy', loadChildren: () => import('./modules/casestudy/casestudy.module').then(m => m.CasestudyModule) },
  {
    path: 'contactus', loadChildren: () => import('./modules/contactus/contactus.module').then(m => m.ContactusModule)
  },
  {
    path: 'applyjob', loadChildren: () => import('./modules/applyjob/applyjob.module').then(m => m.ApplyjobModule)
  },
  {
    path: 'submitdetails', loadChildren: () => import('./modules/submitdetails/submitdetails.module').then(m => m.SubmitdetailsModule)
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  },
];

// @ts-ignore
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
