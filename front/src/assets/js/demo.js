/**
 * Particleground demo
 * @author Jonathan Nicol - @mrjnicol
 */

$(document).ready(function() {
  $('#particles').particleground({
    dotColor: '#30424a1f',
    lineColor: '#30424a1f'
  });
  $('.intro').css({
    // 'margin-top': -($('.intro').height() / 2)
    'margin-top': -($('.intro').height() / 3)
  });
});